#!/bin/bash

# get Tendermint
REPO=github.com/tendermint/tendermint
BRANCH=master
GO_VERSION=1.11.2

sudo apt-get update -y
sudo apt-get install -y make

curl -O https://storage.googleapis.com/golang/go$GO_VERSION.linux-amd64.tar.gz
tar -xvf go$GO_VERSION.linux-amd64.tar.gz

sudo mv go /usr/local
echo "export PATH=\$PATH:/usr/local/go/bin" >> ~/.profile

mkdir go
echo "export GOPATH=$HOME/go" >> ~/.profile
echo "export PATH=\$PATH:\$GOPATH/bin" >> ~/.profile

source ~/.profile

go get $REPO
cd "$GOPATH/src/$REPO"

git checkout $BRANCH
make get_tools
make get_vendor_deps
make install 
make build-linux


