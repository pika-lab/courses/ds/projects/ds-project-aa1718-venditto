package peer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.gossip.GossipGrpc;
import peer.proto.DebuggerGrpc;
import peer.proto.EndorserGrpc;

/**
 * entry point for consuming gossip and/or endorsement rpc services exposed by a peer
 */
public class PeerClient extends AbstractRpcClient {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final GossipGrpc.GossipStub gossipAsyncStub;
    private final GossipGrpc.GossipBlockingStub gossipSyncStub;
    private final EndorserGrpc.EndorserStub endorserStub;
    private final DebuggerGrpc.DebuggerStub debugStub;

    public PeerClient(String host, int port) {
        super(host, port);
        gossipAsyncStub = GossipGrpc.newStub(channel);
        gossipSyncStub = GossipGrpc.newBlockingStub(channel);
        endorserStub = EndorserGrpc.newStub(channel);
        debugStub = DebuggerGrpc.newStub(channel);
    }

    public GossipGrpc.GossipStub getGossipAsyncStub() {return gossipAsyncStub;}

    public GossipGrpc.GossipBlockingStub getGossipStub() {return gossipSyncStub;}

    public EndorserGrpc.EndorserStub getEndorserStub() {return endorserStub;}

    public DebuggerGrpc.DebuggerStub getDebugStub() {return debugStub; }

}