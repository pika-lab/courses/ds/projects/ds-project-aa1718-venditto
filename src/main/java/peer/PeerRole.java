package peer;

public enum PeerRole {
    GOSSIP_MEMBER,  // a peer part of the data dissemination network
    ENDORSER,       // a peer able to handle endorsement requests
    COMMITTER       // a peer interfaced with a block-chain
}
