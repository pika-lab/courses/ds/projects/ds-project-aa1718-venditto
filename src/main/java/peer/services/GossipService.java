package peer.services;

import contract.simulation.WriteSetEntry;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.gossip.*;
import state.WorldState;
import utils.ProtoUtils;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static utils.ProtoUtils.toCompactString;

/**
 * An rpc service that is implemented by a {@link peer.PeerServer} in case the input {@link peer.Peer} has the
 * GOSSIP_MEMBER {@link peer.PeerRole}. this service sends feedback when an update is received and eventually
 * applies the update to the local state.
 */
public class GossipService extends GossipGrpc.GossipImplBase {

    private static final int DEFAULT_PUSH_RATE = 2;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private GossipMember gossipMember;

    public GossipService() {
        this(new SIRGossipMember());
    }

    public GossipService(GossipMember gossipMember) {
        this.gossipMember = gossipMember;
        this.gossipMember.startGossipAtFixedRate(DEFAULT_PUSH_RATE, TimeUnit.SECONDS);
    }

    private UpdateFeedback computeFeedback(StateUpdate update) {
        final boolean accepted = gossipMember.onUpdate(update);
        logger.info("feedback generated => " + accepted);
        return UpdateFeedback.newBuilder()
                .setMetadata(update.getMetadata())
                .setAccepted(accepted).build();
    }

    private void validateAndUpdateState(StateUpdate update) {
        final Map<String,Long> versionDump = WorldState.getInstance().versionSnapshot();
        final boolean canApplyUpdate = update.getReadList().stream()
                .allMatch(e -> !versionDump.containsKey(e.getKey()) || e.getVersion() == versionDump.get(e.getKey()));
        logger.info("check update consistency: can apply: " + canApplyUpdate);
        if (canApplyUpdate) {
            final Set<WriteSetEntry<ByteBuffer>> writeSet = update.getWriteList().stream()
                    .map(ProtoUtils::toWriteSetEntry)
                    .collect(Collectors.toSet());
            WorldState.getInstance().commit(writeSet);
        }
    }

    @Override
    public void commitUpdate(StateUpdate request, StreamObserver<Empty> responseObserver) {
        gossipMember.onUpdate(request);
        validateAndUpdateState(request);
        responseObserver.onNext(Empty.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public StreamObserver<StateUpdate> pushUpdate(StreamObserver<UpdateFeedback> responseObserver) {

        return new StreamObserver<StateUpdate>() {
            @Override
            public void onNext(StateUpdate value) {
                final UpdateFeedback feedback = computeFeedback(value);
                final String metaStr = toCompactString(value.getMetadata());
                logger.info("sending back feedback for: " + metaStr + " => " + feedback.getAccepted());
                responseObserver.onNext(feedback);
                if (feedback.getAccepted()) {
                    logger.info("new update, trying to commit: " + metaStr);
                    validateAndUpdateState(value);
                }
            }

            @Override
            public void onError(Throwable t) {
                logger.error("error on request handling: ", t);
            }

            @Override
            public void onCompleted() {
                logger.debug("communication completed");
            }
        };
    }

    void setGossipMember(GossipMember gossipMember) {
        this.gossipMember = gossipMember;
    }
}
