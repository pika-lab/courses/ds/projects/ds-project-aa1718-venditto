package peer.services;

import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import peer.gossip.Empty;
import peer.gossip.GossipMember;
import peer.gossip.SIRGossipMember;
import peer.gossip.StateEntry;
import peer.proto.DebugGossipMessage;
import peer.proto.DebuggerGrpc;
import state.WorldState;

import java.util.Map;

public class DebugService extends DebuggerGrpc.DebuggerImplBase {

    private GossipMember gossipMember;

    public void setGossipMember(GossipMember gm) {
        gossipMember = gm;
    }

    @Override
    public void dumpState(Empty request, StreamObserver<StateEntry> responseObserver) {
        final Map<String,Long> vDump = WorldState.getInstance().versionSnapshot();
        WorldState.getInstance().dumpRaw().stream()
                .map(e -> StateEntry.newBuilder()
                        .setKey(e.getKey())
                        .setValue(ByteString.copyFrom(e.getValue()))
                        .setVersion(vDump.get(e.getKey())).build()
                ).forEach(responseObserver::onNext);
        responseObserver.onCompleted();
    }

    @Override
    public void dumpGossipMessageQueue(Empty request, StreamObserver<DebugGossipMessage> responseObserver) {
        if (gossipMember != null && gossipMember instanceof SIRGossipMember) {
            final SIRGossipMember sgm = (SIRGossipMember) gossipMember;
            final Map<String, SIRGossipMember.GossipMessage> messages = sgm.getMessageBuffer();
            messages.entrySet().stream().map(m -> DebugGossipMessage.newBuilder()
                    .setMetadata(m.getValue().getMessage().getMetadata())
                    .setUpdate(m.getValue().getMessage())
                    .setSpreading(m.getValue().isSpreading())
                    .setRecvTimestamp(m.getValue().getRecvTimestamp())
                    .build()).forEach(responseObserver::onNext);
        }
        responseObserver.onCompleted();
    }
}
