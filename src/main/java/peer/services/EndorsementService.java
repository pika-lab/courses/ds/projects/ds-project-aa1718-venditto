package peer.services;

import com.google.common.eventbus.EventBus;
import io.grpc.stub.StreamObserver;
import peer.Peer;
import peer.endorsement.DefaultEndorsementRequestHandler;
import peer.endorsement.EndorsementProposalHandler;
import peer.proto.EndorserGrpc;
import peer.proto.Proposal;
import peer.proto.ProposalResponse;

/**
 * An rpc service that is implemented by a {@link peer.PeerServer} in case the input {@link peer.Peer} has the
 * ENDORSER {@link peer.PeerRole}. this services handles endorsement requests.
 */
public class EndorsementService extends EndorserGrpc.EndorserImplBase {

    private EndorsementProposalHandler endorsementHandler;
    private final Peer endorser;

    public EndorsementService(Peer peer) {
        this.endorsementHandler = new DefaultEndorsementRequestHandler();
        this.endorser = peer;
    }

    @Override
    public void submitForEndorsement(Proposal request, StreamObserver<ProposalResponse> responseObserver) {
        final ProposalResponse response = endorsementHandler.buildResponse(request, endorser);
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    public void setEndorsementHandler(EndorsementProposalHandler handler) {
        this.endorsementHandler = handler;
    }
}
