package peer.gossip;

import peer.Peer;

import java.util.Iterator;
import java.util.List;

/**
 * represents a strategy to arbitrary selects a group of peers
 */
public interface PeerSelection {

    Iterator<Peer> select(List<Peer> peers);

}
