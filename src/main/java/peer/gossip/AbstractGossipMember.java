package peer.gossip;

import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.Peer;
import peer.PeerClient;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;

import static utils.ProtoUtils.toCompactString;

/**
 * partial implementation of {@link GossipMember}.
 */
public abstract class AbstractGossipMember implements GossipMember {

    protected PeerSelection peerSelection;
    protected LossOfInterest loi;
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    protected static final int ENTRY_LOG_LIMIT = 5;

    public abstract boolean onUpdate(StateUpdate update);

    public abstract void onFeedback(UpdateFeedback feedback);

    @Override
    public void spreadUpdates(Iterator<StateUpdate> updates) {
        if (!updates.hasNext()) return;

        final List<Peer> peers = PeerManager.getInstance().getAllPeers();
        if (peers.size() < 1) {
            logger.warn("cannot disseminate, no peers available a the moment.");
            return;
        }

        final Peer peer = peerSelection.select(peers).next();
        logger.info("push update to: " + peer.address().getHostName() + ":" + peer.address().getPort());
        final Optional<PeerClient> client = PeerManager.getInstance().getPeerClient(peer.id());

        if (!client.isPresent()) {
            logger.error("trying to contact an unknown peer: " + peer);
            return;
        }

        final CountDownLatch completedLatch = new CountDownLatch(1);
        final StreamObserver<UpdateFeedback> feedbackObserver = new StreamObserver<UpdateFeedback>() {
            @Override
            public void onNext(UpdateFeedback value) { onFeedback(value); }

            @Override
            public void onError(Throwable t) {
                logger.error("onFeedback failed: cause: " + t.toString());
                completedLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.error("onFeedback completed.");
                completedLatch.countDown();
            }
        };

        final StreamObserver<StateUpdate> requestObserver =
                client.get().getGossipAsyncStub().pushUpdate(feedbackObserver);

        updates.forEachRemaining(u -> {
            logger.info("pushing update: " + toCompactString(u, ENTRY_LOG_LIMIT)  + " => " + peer.address());
            requestObserver.onNext(u);
            logger.info(completedLatch + "");
        });

        requestObserver.onCompleted();
    }

    @Override
    public void setPeerSelectionStrategy(PeerSelection peerSelection) {
        this.peerSelection = peerSelection;
    }

    @Override
    public void setLossOfInterestStrategy(LossOfInterest lossOfInterest) {
        this.loi = lossOfInterest;
    }
}
