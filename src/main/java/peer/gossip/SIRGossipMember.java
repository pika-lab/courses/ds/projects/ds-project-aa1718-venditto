package peer.gossip;

import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import utils.MapUtils;

import java.time.Instant;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static utils.ProtoUtils.toCompactString;

/**
 * A basic implementation of a data dissemination algorithm, managing
 * multiple updates. Each update is initially unknown to the peer when it receives it
 * for the first time. Upon first reception the update transitions to a state in which it's spread to
 * other known peers, selected by a pluggable strategy. Eventually the peer will stop spreading an
 * update, the decision to stop disseminating an update it's based on a feedback-aware strategy.
 */
public class SIRGossipMember extends AbstractGossipMember {

    private Map<String, GossipMessage> receivedMessages;
    private ScheduledFuture<?> gossipTask;

    public SIRGossipMember() {
        this.receivedMessages = Maps.newHashMap();
        this.peerSelection = new RandomPeerSelection();
        this.loi = new ProbabilisticLossOfInterest(5);
    }

    public Map<String, GossipMessage> getMessageBuffer() {
        return ImmutableMap.copyOf(receivedMessages);
    }

    @Override
    public boolean onUpdate(StateUpdate update) {
        logger.info("update received: \n" + toCompactString(update, ENTRY_LOG_LIMIT));
        final Metadata info = update.getMetadata();
        final String id = info.getId() + info.getTimestamp();
        if (receivedMessages.containsKey(id)) {
            return false;
        } else {
            receivedMessages.put(id, new GossipMessage(update));
            return true;
        }
    }

    @Override
    public void onFeedback(UpdateFeedback feedback) {
        logger.info("feedback received: was update accepted: " + feedback.getAccepted());
        final String id = feedback.getMetadata().getId() + feedback.getMetadata().getTimestamp();
        final GossipMessage m = receivedMessages.get(id);
        logger.info("message for id: " + id + " => " + m);
        if (m != null) {
            final boolean stopSpreding = loi.shouldStopSpreading(m.getMessage());
            logger.info("stop spreading? " + toCompactString(feedback.getMetadata()) + " => " + stopSpreding);
            m.setSpreading(!stopSpreding);
        }
    }

    public void startGossipAtFixedRate(int rateSeconds, TimeUnit unit) {
        final ScheduledExecutorService execService = Executors.newSingleThreadScheduledExecutor();
        this.stopGossip();
        this.gossipTask = execService.scheduleAtFixedRate(() -> {
            final Iterator<StateUpdate> updates = receivedMessages.values().stream()
                    .filter(GossipMessage::isSpreading)
                    .map(GossipMessage::getMessage).iterator();
            logger.debug("has messages marked for spreading: ", updates.hasNext());
            spreadUpdates(updates);
        }, 0, rateSeconds, TimeUnit.SECONDS);
    }

    public void stopGossip() {
        if (gossipTask != null && !gossipTask.isCancelled()) {
            gossipTask.cancel(true);
        }
    }

    public static class GossipMessage {

        private boolean spreading = true;
        private StateUpdate message;
        private final long recvTimestamp = Instant.now().toEpochMilli();

        GossipMessage(StateUpdate message) {
            this.message = message;
        }

        public StateUpdate getMessage() {
            return message;
        }

        public long getRecvTimestamp() {
            return recvTimestamp;
        }

        public boolean isSpreading() {
            return spreading;
        }

        public void setSpreading(Boolean spreading) {
            this.spreading = spreading;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            GossipMessage that = (GossipMessage) o;
            final Metadata thatInfo = that.getMessage().getMetadata();
            final Metadata thisInfo = message.getMetadata();
            boolean result = true;
            result = result && thatInfo.getId().equals(thisInfo.getId());
            result = result && thatInfo.getTimestamp() == thisInfo.getTimestamp();
            return result;
        }

        @Override
        public int hashCode() {
            final Metadata thisInfo = message.getMetadata();
            return Objects.hash(thisInfo.getId(), thisInfo.getTimestamp());
        }
    }
}
