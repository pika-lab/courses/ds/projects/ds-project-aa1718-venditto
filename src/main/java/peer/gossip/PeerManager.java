package peer.gossip;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.Peer;
import peer.PeerClient;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/*
* a manager class keeping track of connections to other peers known by the local peer.
* */
public final class PeerManager {

    private Map<String, Peer> knownPeers;

    private Map<String, PeerClient> peerConnections;

    private static PeerManager INSTANCE;

    private static final Logger logger = LoggerFactory.getLogger("peer-manager");

    private PeerManager() {
        this.knownPeers = Maps.newConcurrentMap();
        this.peerConnections = Maps.newConcurrentMap();
    }

    public static synchronized PeerManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PeerManager();
        }
        return INSTANCE;
    }

    public Optional<Peer> getPeer(String id) {
        return Optional.ofNullable(knownPeers.get(id));
    }

    public Optional<PeerClient> getPeerClient(String id) { return Optional.ofNullable(peerConnections.get(id)); }

    public PeerManager addPeer(Peer peer) {
        logger.info("registered: " + peer);
        knownPeers.put(peer.id(), peer);
        if (peerConnections.containsKey(peer.id())) {
            peerConnections.get(peer.id()).close();
        }
        peerConnections.put(peer.id(), new PeerClient(
                peer.address().getAddress().getHostAddress(),
                peer.address().getPort()));
        return this;
    }

    public List<Peer> getAllPeers() {
        return Lists.newArrayList(knownPeers.values());
    }
}
