package peer.gossip;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import peer.Peer;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * return a random ordered iterator of the input peers collection
 */
public class RandomPeerSelection implements PeerSelection {

    private static final int N = 1; //TODO make a parameter rather then a fixed value

    @Override
    public Iterator<Peer> select(List<Peer> peers) {
        final List<Peer> randomOrdered = Lists.newArrayList(peers);
        Collections.shuffle(randomOrdered);
        return Iterables.limit(randomOrdered, N).iterator();
    }
}
