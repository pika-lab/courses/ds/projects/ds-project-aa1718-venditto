package peer.gossip;

import java.util.Random;


/*
* suggests to stop spreading an update with a probability of 1/k,
* where k should approximately be the times an update has been pushed to
* and aknowledged by other peers.
* */
public class ProbabilisticLossOfInterest implements LossOfInterest {

    private int k;
    private final Random rand;

    public ProbabilisticLossOfInterest(int k) {
        this.k = k;
        this.rand = new Random();
    }

    @Override
    public boolean shouldStopSpreading(StateUpdate update) {
        return rand.nextInt(k) == 0;
    }
}
