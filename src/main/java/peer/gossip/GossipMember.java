package peer.gossip;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/*
* models an entity participating to a gossip-based data dissemination protocol.
* */
public interface GossipMember {

    void spreadUpdates(Iterator<StateUpdate> update);

    boolean onUpdate(StateUpdate update);

    void onFeedback(UpdateFeedback feedback);

    void startGossipAtFixedRate(int period, TimeUnit unit);

    void stopGossip();

    void setPeerSelectionStrategy(PeerSelection peerSelection);

    void setLossOfInterestStrategy(LossOfInterest lossOfInterest);

}
