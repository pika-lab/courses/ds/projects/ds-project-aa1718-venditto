package peer.gossip;

/*
* represents a strategy to be use by a peer participating to
* gossip to decide whenever stop spreading an update.
* */
public interface LossOfInterest {

    boolean shouldStopSpreading(StateUpdate update);

}
