package peer;

import com.google.common.collect.Sets;

import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class PeerImpl implements Peer {

    private final InetSocketAddress address;
    private final String id;
    private final Set<PeerRole> roles;

    public PeerImpl(String id, String host, int port) {
        this.id = id;
        this.roles = Collections.synchronizedSet(Sets.newEnumSet(Sets.newHashSet(), PeerRole.class));
        this.address = new InetSocketAddress(host, port);
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public InetSocketAddress address() {
        return address;
    }

    @Override
    public void addRole(PeerRole role) {
        roles.add(role);
    }

    @Override
    public void addRoles(PeerRole... rolesToAdd) {
        roles.addAll(Sets.newHashSet(rolesToAdd));
    }

    @Override
    public void removeRole(PeerRole role) {
        roles.remove(role);
    }

    @Override
    public Set<PeerRole> getRoles() {
        return Sets.immutableEnumSet(roles);
    }

    @Override
    public boolean hasRole(PeerRole role) {
        return roles.contains(role);
    }

    @Override
    public String toString() {
        return id + "@" + address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PeerImpl peer = (PeerImpl) o;
        return id.equals(peer.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
