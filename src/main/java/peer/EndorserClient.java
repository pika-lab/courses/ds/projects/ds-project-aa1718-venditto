package peer;

import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.endorsement.EndorsementProposal;
import peer.proto.BroadcastProposalTx;
import peer.proto.Proposal;
import peer.proto.ProposalResponse;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * an client able to handle {@link EndorsementProposal}s
 */
public class EndorserClient implements Closeable {

    private ScheduledExecutorService scheduler;
    private int endorsementResponseTimeoutSeconds = 5;
    private BroadcastTxClient broadcastClient;

    private final Logger logger = LoggerFactory.getLogger(getClass());


    public EndorserClient() {
        this("localhost", 26659);
    }

    public EndorserClient(String commitAddr, int commitPort) {
        broadcastClient = new BroadcastTxClient(commitAddr, commitPort);
        scheduler = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors() + 1);
    }

    public CompletableFuture<List<ProposalResponse>> submitForEndorsement(EndorsementProposal endorsementProposal) {

        final Proposal proposal = endorsementProposal.getProposal();

        final List<CompletableFuture<ProposalResponse>> asyncEndorsements = endorsementProposal.getEndorsers()
                .stream()
                .map(p -> askForEndorsement(proposal, p))
                .collect(Collectors.toList());

        final CompletableFuture<Void> allFutures = CompletableFuture.allOf(
                asyncEndorsements.toArray(new CompletableFuture[0]));

        final CompletableFuture<List<ProposalResponse>> allEndorsements = allFutures.thenApply(endorsements ->
                asyncEndorsements.stream()
                    .map(CompletableFuture::join)
                    .collect(Collectors.toList())
        );

        return allEndorsements.thenApplyAsync(endorsements -> {
           onAllEndorsementsReceived(endorsementProposal, endorsements);
           return endorsements;
        });
    }

    private void onAllEndorsementsReceived(EndorsementProposal endorsementProposal, List<ProposalResponse> endorsements) {

        final Proposal proposal = endorsementProposal.getProposal();

        try {
            final byte[] proposalHashBytes = MessageDigest.getInstance("SHA-256").digest(proposal.toByteArray());
            final ByteString proposalHash = ByteString.copyFrom(proposalHashBytes);

            final List<ProposalResponse> positiveResponses = endorsements.stream()
                    .filter(e -> e.getStatus() >= 0 && e.getProposalHash().equals(proposalHash))
                    .collect(Collectors.toList());

            if (endorsementProposal.getEndorsementPolicy().check(positiveResponses)) {
                logger.info("endorsement policy satisfied, broadcasting tx.");
                final BroadcastProposalTx.Builder txBuilder = BroadcastProposalTx.newBuilder()
                        .setProposal(proposal);
                endorsements.forEach(txBuilder::addEndorsements);
                broadcastClient.broadcastTx(txBuilder.build().toByteArray());
            } else {
                logger.info("endorsement policy not satisfied. nothing to do.");
            }
        } catch (NoSuchAlgorithmException e) {
            logger.error("error while verifying endorsements: cause: " + e);
        }
    }

    private CompletableFuture<ProposalResponse> askForEndorsement(Proposal proposal, Peer endorser) {

        final CompletableFuture<ProposalResponse> futureEndorsement = new CompletableFuture<>();

        final InetSocketAddress addr = endorser.address();
        final PeerClient endorserClient = new PeerClient(addr.getHostString(), addr.getPort());

        endorserClient.getEndorserStub().submitForEndorsement(proposal, new StreamObserver<ProposalResponse>() {
            @Override
            public void onNext(ProposalResponse value) {
                futureEndorsement.complete(value);
            }

            @Override
            public void onError(Throwable t) {
                futureEndorsement.completeExceptionally(t);
            }

            @Override
            public void onCompleted() {
                endorserClient.close();
            }
        });

        scheduler.schedule(
                () -> futureEndorsement.completeExceptionally(new TimeoutException()),
                endorsementResponseTimeoutSeconds, TimeUnit.SECONDS);

        return futureEndorsement;
    }

    @Override
    public void close() throws IOException {
        broadcastClient.close();
    }
}
