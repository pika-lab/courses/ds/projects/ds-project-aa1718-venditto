package peer;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import com.google.protobuf.ByteString;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.gossip.Empty;
import peer.gossip.GossipMember;
import peer.gossip.SIRGossipMember;
import peer.gossip.StateEntry;
import peer.proto.DebuggerGrpc;
import peer.services.DebugService;
import peer.services.EndorsementService;
import peer.services.GossipService;
import state.WorldState;

import java.io.IOException;
import java.util.Map;


/**
 * An rpc server exposing peer services based on the input peer roles.
 */
public class PeerServer {

    private final Server server;
    private final Logger logger;

    public PeerServer(Peer peer) {
        this(peer, true);
    }

    public PeerServer(Peer peer, boolean debugMode) {
        logger = LoggerFactory.getLogger("server["+peer.id()+"]");

        final ServerBuilder serverBuilder = NettyServerBuilder.forAddress(peer.address());

        GossipMember gossipMember = null;
        if (peer.hasRole(PeerRole.GOSSIP_MEMBER)) {
            gossipMember = new SIRGossipMember();
            serverBuilder.addService(new GossipService(gossipMember));
        }

        if (peer.hasRole(PeerRole.ENDORSER)) {
            serverBuilder.addService(new EndorsementService(peer));
        }

        if (debugMode) {
            final DebugService ds = new DebugService();
            ds.setGossipMember(gossipMember);
            serverBuilder.addService(ds);
        }

        server = serverBuilder.build();

        logger.info("ready, listening on port: " + peer.address().getPort());

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.err.println("JVM is shutting down, shutting down peer.");
            PeerServer.this.stop();
            System.err.println("server was shut down");
        }));
    }

    public void start() throws IOException {
        if (server != null) {
            server.start();
        }
    }

    public void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon threads.
     */
    public void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }
}
