package peer;

import java.net.InetSocketAddress;
import java.util.Set;


public interface Peer {

    String id();

    InetSocketAddress address();

    void addRole(PeerRole role);

    void addRoles(PeerRole... roles);

    void removeRole(PeerRole role);

    Set<PeerRole> getRoles();

    boolean hasRole(PeerRole role);

}
