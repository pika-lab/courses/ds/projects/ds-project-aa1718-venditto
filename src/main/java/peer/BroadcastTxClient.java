package peer;

import com.google.protobuf.ByteString;
import core_grpc.BroadcastAPIGrpc;
import core_grpc.RequestBroadcastTx;
import core_grpc.ResponseBroadcastTx;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * rpc client used to broadcast a transaction to the blockchain netwprk
 */
public class BroadcastTxClient extends AbstractRpcClient  {

    private final BroadcastAPIGrpc.BroadcastAPIStub stub;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public BroadcastTxClient(String host, int port) {
        super(host, port);
        this.stub = BroadcastAPIGrpc.newStub(channel);
    }

    public void broadcastTx(byte[] txBytes) {

        final RequestBroadcastTx tx = RequestBroadcastTx.newBuilder()
                .setTx(ByteString.copyFrom(txBytes))
                .build();

        stub.broadcastTx(tx, new StreamObserver<ResponseBroadcastTx>() {
            @Override
            public void onNext(ResponseBroadcastTx value) {
                logger.info("broadcastTx response: " + value);
            }

            @Override
            public void onError(Throwable t) {
                t.printStackTrace();
                System.err.println("error on broadcastTx: " + t);
            }

            @Override
            public void onCompleted() {
                System.out.println("completed broadcastTx!");
            }
        });
    }
}
