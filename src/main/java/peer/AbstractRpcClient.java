package peer;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.io.Closeable;
import java.util.concurrent.TimeUnit;


public abstract class AbstractRpcClient implements Closeable {

    protected ManagedChannel channel;
    protected long shutdownTimeoutSeconds = 5;

    public AbstractRpcClient(String host, int port) {
        this.channel = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext()
                .build();
    }

    @Override
    public void close() {
        try {
            channel.shutdown().awaitTermination(shutdownTimeoutSeconds, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
