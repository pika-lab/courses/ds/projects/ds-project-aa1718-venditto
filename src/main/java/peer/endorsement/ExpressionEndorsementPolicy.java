package peer.endorsement;

import com.google.common.collect.Lists;
import org.apache.commons.jexl3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.proto.ProposalResponse;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * An endorsement policy created from a boolean expression.
 *  - endorsers in the policy must be specified in the form <peer_ID>.
 *  - <peer_id> cannot contain characters matching with operators TODO fix
 * eg. (peer.A && peer.B) || (peer.C && peer.D) would be a valid policy.
 *
 *
 * TODO: restrict the parser to only consider boolean operators to avoid issues
 **/
public class ExpressionEndorsementPolicy implements EndorsementPolicy {

    private final JexlEngine jexl;
    private JexlExpression policyExpr;
    private final Pattern pattern = Pattern.compile("(?:peer|)\\.([a-zA-Z\\_\\d]*)");  //((peer|)_\w+)

    /* the endorsers specified in the input expression */
    private final List<String> variables;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private ExpressionEndorsementPolicy(String policy) {
        jexl = new JexlBuilder().create();
        this.policyExpr = jexl.createExpression(policy);
        this.variables = Lists.newArrayList();
        extractPolicyMembers(policy);
    }

    public static EndorsementPolicy parseFrom(String expr) {
        return new ExpressionEndorsementPolicy(expr);
    }

    /**
     * searches the input expression string for entries
     * in the form (peer|)_ID and populate the variables list.
     * */
    private void extractPolicyMembers(String policyExpr) {
        final Matcher matcher = pattern.matcher(policyExpr);
        while(matcher.find()) {
            variables.add(matcher.group());
        }
        logger.info("policy variables: " + variables);
    }

    @Override
    public boolean check(List<ProposalResponse> endorsements) {
        try {
            logger.info("checking: " + endorsements);
            final JexlContext ctx = new MapContext();
            // for every endorser found by this::extractPolicyMembers,
            // set the relative variable in the JEXL context with
            // a boolean value indicating if a valid endorsement from
            // this peer is present in the endorsements list or not
            variables.forEach(v -> {
                final String endorserID = v.split("\\.")[1];
                logger.info("binding variable: " + v + " : endorserID => " + endorserID);
                ctx.set(v, endorsements.stream().anyMatch(
                        e -> e.getStatus() >= 0 && e.getEndorserID().equals(endorserID)));
            });
            return (boolean) policyExpr.evaluate(ctx);
        } catch (Exception e) {
            logger.error("error evaluating endorsement policy: cause: " + e);
            return false;
        }
    }
}
