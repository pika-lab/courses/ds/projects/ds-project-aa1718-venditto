package peer.endorsement;

import com.google.common.collect.ImmutableSet;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import contract.SmartContractRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.Peer;
import peer.gossip.StateEntry;
import peer.proto.DeployPayload;
import peer.proto.Proposal;
import peer.proto.ProposalResponse;
import peer.proto.ResponsePayload;
import state.Namespaces;
import utils.ProtoUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Set;

import static javax.xml.bind.DatatypeConverter.printHexBinary;
import static utils.CliUtil.ellipsize;


/**
 * Validate a deployment requests verifying the payload is well-formed and
 * the attributes of the smart contract to deploy are defined.
 */
public class DeployHandler implements EndorsementProposalHandler {

    private final SmartContractRepository repo = SmartContractRepository.getInstance();
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Set<String> mandatoryFields = ImmutableSet.of("name", "source", "version");
    private MessageDigest hash;

    private static final String INVALID_PAYLOAD_ERR = "request type is DEPLOY, but cannot parse payload.";

    public DeployHandler() {
        try {
            hash = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            logger.error("missing required hash algorithm implementation!");
        }
    }

    @Override
    public ProposalResponse buildResponse(Proposal proposal, Peer endorser) {

        final DeployPayload newContract = verify(proposal);
        final ProposalResponse.Builder responseBuilder =
                ProposalResponse.newBuilder().setEndorserID(endorser.id());

        if (newContract != null && !repo.getContract(newContract.getName()).isPresent()) {

            final String namespacedName = Namespaces.INTERNAL_SMART_CONTRACT.namespaced(newContract.getName());
            final StateEntry write = StateEntry.newBuilder()
                    .setKey(namespacedName)
                    .setValue(newContract.toByteString())
                    .setVersion(0)
                    .build();

            final ResponsePayload rwSet = ResponsePayload.newBuilder()
                    .addWrite(write)
                    .build();

            return responseBuilder
                    .setStatus(StatusCode.OK.ordinal())
                    .setProposalHash(ByteString.copyFrom(hash.digest(proposal.toByteArray())))
                    .setPayload(rwSet.toByteString())
                    .setTimestamp(Instant.now().toEpochMilli())
                    .build();
        }

        return responseBuilder
                .setStatus(StatusCode.FAILURE.ordinal())
                .setTimestamp(Instant.now().toEpochMilli())
                .build();
    }

    private DeployPayload verify(Proposal request) {
        logger.info("is proposal valid?: " + request);
        final DeployPayload newContract = parsePayload(request);
        final boolean isValid = newContract != null && ProtoUtils.hasFields(newContract, mandatoryFields);
        return isValid ? newContract : null;
    }

    private DeployPayload parsePayload(Proposal request) {
        try {
            final byte[] payload = request.getPayload().toByteArray();
            final String logPayloadBytes = ellipsize(printHexBinary(payload), 15, true);
            logger.info("payload bytes: " + logPayloadBytes);
            return DeployPayload.parseFrom(payload);
        } catch (InvalidProtocolBufferException e) {
            logger.error(INVALID_PAYLOAD_ERR);
        }
        return null;
    }
}
