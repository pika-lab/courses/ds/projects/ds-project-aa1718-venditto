package peer.endorsement;

import com.google.common.collect.ImmutableSet;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import contract.simulation.InvocationRequest;
import contract.simulation.InvocationResult;
import contract.simulation.Invoker;
import contract.simulation.SimplePrologInvoker;
import exceptions.NoSuchSmartContractException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.Peer;
import peer.proto.InvokePayload;
import peer.proto.Proposal;
import peer.proto.ProposalResponse;
import peer.proto.ResponsePayload;
import utils.ProtoUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static javax.xml.bind.DatatypeConverter.printHexBinary;
import static utils.CliUtil.ellipsize;

/**
 * handler for an endorsement proposal holding an invocation request
 */
public class InvokeHandler implements EndorsementProposalHandler {

    private final Set<String> mandatoryFields = ImmutableSet.of("contractID", "version", "args");
    private MessageDigest hash;

    private final Logger logger = LoggerFactory.getLogger("endorse-handler");

    private static final String OP_ARG = "op";
    private static final String INVALID_PAYLOAD_ERR = "request type is INVOKE, but cannot parse payload.";
    private static final String EXECUTION_ERR = "an error happened while execution the invocation.";

    public InvokeHandler() {
        try {
            hash = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            logger.error("missing required hash algorithm implementation!");
        }
    }

    @Override
    public ProposalResponse buildResponse(Proposal proposal, Peer endorser) {
        // try to parse the payload
        final InvokePayload invoke = verify(proposal);
        final ProposalResponse.Builder responseBuilder = ProposalResponse.newBuilder();
        // return a failure if payload is not a valid InvokePayload
        if (invoke == null) return failure(responseBuilder, INVALID_PAYLOAD_ERR);

        final InvocationRequest.Builder reqBuilder = parseArgs(invoke);
        final InvocationResult<String> result = execute(reqBuilder);
        final ResponsePayload.Builder payload = ResponsePayload.newBuilder();

        // if the execution was successful, build a response containing read/write sets
        // TODO future: implement endorsement logic (signatures, etc.)
        if (result != null) {
            result.getReadSet().stream().map(ProtoUtils::toStateEntry).forEach(payload::addRead);
            result.getWriteSet().stream().map(ProtoUtils::toStateEntry).forEach(payload::addWrite);
            logger.info("generated r/w-set:\n" + payload);
            return responseBuilder
                    .setEndorserID(endorser.id())
                    .setStatus(StatusCode.OK.ordinal())
                    .setProposalHash(ByteString.copyFrom(hash.digest(proposal.toByteArray())))
                    .setPayload(payload.build().toByteString())
                    .setTimestamp(Instant.now().toEpochMilli())
                    .build();
        }
        return failure(responseBuilder, EXECUTION_ERR);
    }

    private ProposalResponse failure(ProposalResponse.Builder responseBuilder, String reason) {
        return responseBuilder
                .setStatus(StatusCode.FAILURE.ordinal())
                .setMessage(reason)
                .build();
    }

    private InvocationResult<String> execute(InvocationRequest.Builder reqBuilder) {
        final InvocationRequest invokeReq = reqBuilder.build();
        logger.info("built invocation request: " + invokeReq);
        final Invoker<String> invoker = new SimplePrologInvoker();
        try {
            // TODO: bring timeout value out of this method
            return invoker.execute(invokeReq).get(5, TimeUnit.SECONDS);
        } catch (NoSuchSmartContractException | ExecutionException | TimeoutException | InterruptedException e) {
            logger.error("error while processing invocation request: "+ invokeReq + " cause: " + e);
        }
        return null;
    }

    /**
     * parse the InvokePayload as a JSON-encoded string of arguments and generate a builder
     * for the InvocationRequest with the parsed parameters
     */
    private InvocationRequest.Builder parseArgs(InvokePayload invoke) {
        final JsonObject args = new JsonParser().parse(invoke.getArgs()).getAsJsonObject();
        final String opToInvoke = args.get(OP_ARG).getAsString();
        final InvocationRequest.Builder reqBuilder =
                InvocationRequest.builder(invoke.getContractID(), opToInvoke);

        args.entrySet().stream()
                .filter(e -> e.getValue().isJsonPrimitive() && !e.getKey().equals(OP_ARG))
                .forEach(e -> {
                    final String name = e.getKey();
                    final JsonPrimitive val = e.getValue().getAsJsonPrimitive();
                    logger.info("precessing arg: " + name + ":" + val);
                    if (val.isString()) {
                        reqBuilder.paramString(name, val.getAsString());
                    } else if (val.isBoolean()) {
                        reqBuilder.paramBoolean(name, val.getAsBoolean());
                    } else if (val.isNumber()) {
                        final Number num = val.getAsNumber();
                        reqBuilder.paramNumber(name, num);
                    }
                });
        return reqBuilder;
    }

    /**
     * a Proposal is a valid invocation proposal if the payload is a valid InvokePayload,
     * thus can per successfully parsed and contains all the mandatory fields
     */
    private InvokePayload verify(Proposal proposal) {
        final InvokePayload invoke = parsePayload(proposal);
        final boolean isValid = (invoke != null && ProtoUtils.hasFields(invoke, mandatoryFields));
        return isValid ? invoke : null;
    }

    private InvokePayload parsePayload(Proposal request) {
        try {
            final byte[] payload = request.getPayload().toByteArray();
            final String logPayloadBytes = ellipsize(printHexBinary(payload), 15, true);
            logger.info("payload bytes: " + logPayloadBytes);
            return InvokePayload.parseFrom(payload);
        } catch (InvalidProtocolBufferException e) {
            logger.error(INVALID_PAYLOAD_ERR + " " + request);
        }
        return null;
    }

}

