package peer.endorsement;

import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.Peer;
import peer.proto.Proposal;
import peer.proto.ProposalResponse;

import java.time.Instant;
import java.util.EnumMap;
import java.util.Map;

public class DefaultEndorsementRequestHandler implements EndorsementProposalHandler {

    private final static Logger logger = LoggerFactory.getLogger("req-router");
    private final static String UNKNOWN_REQ_TYPE_ERR = "unknown request type:";

    private Map<RequestType, EndorsementProposalHandler> handlers;

    public DefaultEndorsementRequestHandler() {
        final EnumMap<RequestType, EndorsementProposalHandler> _handlers = new EnumMap<>(RequestType.class);
         _handlers.put(RequestType.DEPLOY, new DeployHandler());
         _handlers.put(RequestType.INVOKE, new InvokeHandler());
        handlers = ImmutableMap.copyOf(_handlers);
    }

    @Override
    public ProposalResponse buildResponse(Proposal proposal, Peer endorser) {
        final EndorsementProposalHandler handler = getHandlerFor(proposal);
        if (handler != null) {
            return handler.buildResponse(proposal, endorser);
        }
        return ProposalResponse.newBuilder()
                .setEndorserID(endorser.id())
                .setStatus(StatusCode.FAILURE.ordinal())
                .setTimestamp(Instant.now().toEpochMilli())
                .setMessage("unknown request type.")
                .build();
    }

    private EndorsementProposalHandler getHandlerFor(Proposal request) {
        final int reqType = request.getHeader().getType();
        try {
            final RequestType type = RequestType.values()[reqType];
            return handlers.get(type);
        } catch (ArrayIndexOutOfBoundsException e) {
            logger.error(UNKNOWN_REQ_TYPE_ERR, reqType);
        }
        return null;
    }
}
