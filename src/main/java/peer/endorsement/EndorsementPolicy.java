package peer.endorsement;

import peer.proto.ProposalResponse;

import java.util.List;

public interface EndorsementPolicy {

    boolean check(List<ProposalResponse> endorsements);

}
