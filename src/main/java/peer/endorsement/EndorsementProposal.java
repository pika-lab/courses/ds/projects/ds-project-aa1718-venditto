package peer.endorsement;

import peer.Peer;
import peer.proto.Proposal;

import java.util.Set;

public class EndorsementProposal {

    private Set<Peer> endorsers;
    private Proposal proposal;
    private EndorsementPolicy endorsementPolicy;

    public EndorsementProposal(Proposal proposal, Set<Peer> endorsers, EndorsementPolicy endorsementPolicy) {
        this.endorsers = endorsers;
        this.proposal = proposal;
        this.endorsementPolicy = endorsementPolicy;
    }

    public Proposal getProposal() { return proposal; }

    public Set<Peer> getEndorsers() { return endorsers; }

    public EndorsementPolicy getEndorsementPolicy() { return endorsementPolicy; }
}
