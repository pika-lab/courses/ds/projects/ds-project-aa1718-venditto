package peer.endorsement;

public enum RequestType {
    DEPLOY,
    INVOKE,
    QUERY
}
