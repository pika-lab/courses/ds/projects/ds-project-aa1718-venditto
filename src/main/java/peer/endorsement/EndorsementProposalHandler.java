package peer.endorsement;

import peer.Peer;
import peer.proto.Proposal;
import peer.proto.ProposalResponse;

public interface EndorsementProposalHandler {

    ProposalResponse buildResponse(Proposal proposal, Peer endorser);
}
