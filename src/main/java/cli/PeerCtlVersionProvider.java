package cli;

import picocli.CommandLine;

public class PeerCtlVersionProvider implements CommandLine.IVersionProvider {

    private static final String VERSION_LABEL = "peerctl version: ";

    @Override
    public String[] getVersion() throws Exception {
        final String jarVersion = getClass().getPackage().getImplementationVersion();
        return new String[]{VERSION_LABEL + (jarVersion == null ? "DEV" : jarVersion)};
    }
}
