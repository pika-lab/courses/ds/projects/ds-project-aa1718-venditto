package cli;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.Peer;
import peer.PeerImpl;
import peer.PeerRole;
import peer.PeerServer;
import peer.gossip.PeerManager;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParentCommand;
import utils.DockerUtils;
import utils.PeerTypeAdapter;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("Duplicates")
@Command(name="init")
public class PeerInitCmd implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger("peerctl:init");
    private static final String ERR_INIT_PEER = "cannot init peer: cause: ";

    @ParentCommand
    protected PeerCtl peer;

    @Option(names={"--known-peers", "-kp"}, paramLabel = "knownPeers", description = "static known peers (json format file)")
    protected String knownPeers;

    @Option(names={"--name", "-n"}, paramLabel = "name")
    protected String name;

    @Option(names={"--known-peer-docker"}, paramLabel="serviceName:Port", description = "if running as a docker service on top of a docker swarm, discover bootstrap peers with a dns lookup a tasks.<serviceName>")
    private String dockerServiceName;

    protected String randName() {
        return UUID.randomUUID().toString().replace("-", "_");
    }

    @Override
    public void run() {
        final String peerName = name == null ? randName(): name;
        logger.info("init peer: " + peerName);
        final Peer self = new PeerImpl(peerName, peer.host, peer.port);
        this.setRoles(self);

        try {
            if (knownPeers != null || dockerServiceName != null)
                loadKnownPeers(self, knownPeers);
            final PeerServer server = new PeerServer(self);
            server.start();
            server.blockUntilShutdown();
        } catch (IOException | InterruptedException e) {
            System.err.println(ERR_INIT_PEER + e.getMessage());
        }
    }

    protected void setRoles(Peer self) {
        final List<String> roles = peer.peerCfg.roles();
        roles.forEach(role -> {
            logger.info("registering role: " + role);
            switch (role) {
                case "gossip_member":
                    self.addRole(PeerRole.GOSSIP_MEMBER);
                    break;
                case "endorser":
                    self.addRole(PeerRole.ENDORSER);
                    break;
                default:
                    logger.warn("unknown role: " + role);
                    break;
            }
        });
    }

    protected void loadKnownPeers(Peer self, String jsonFile) throws FileNotFoundException {
        try {
            final String localAddr = InetAddress.getLocalHost().getHostAddress();
            logger.info("local host preferred address is: " + localAddr);
            if (dockerServiceName == null) {
                final JsonReader reader = new JsonReader(new FileReader(jsonFile));
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.registerTypeAdapter(
                        Peer.class, new PeerTypeAdapter()).create();
                List<Peer> peerList = gson.fromJson(reader, new TypeToken<List<Peer>>() {
                }.getType());
                peerList.stream()
                        .filter(p -> !p.address().getAddress().getHostAddress().equals(localAddr))
                        .forEach(PeerManager.getInstance()::addPeer);
            } else {
                logger.info("docker mode: try to discover other peers (DNS): " + dockerServiceName);
                final String[] serviceInfo = dockerServiceName.split(":");
                logger.info("wait a bit to have more chances all peers are ready");
                waitAndNoticeSeconds(10);
                if (serviceInfo.length != 2) return;
                final String serviceName = serviceInfo[0];
                final int port = Integer.parseInt(serviceInfo[1]);
                Arrays.stream(DockerUtils.discoverTasks(serviceName))
                        .filter(p -> !p.getHostAddress().equals(localAddr))
                        .map(p -> new PeerImpl(p.getHostName(), p.getHostAddress(), port))
                        .forEach(PeerManager.getInstance()::addPeer);
            }
        } catch (UnknownHostException e) {
            logger.error("cannot register peers: cause: " + e);
        }
    }

    private void waitAndNoticeSeconds(int seconds) {
        try {
            for (int i = 0; i < seconds; i++) {
                logger.info("waiting: remaining: " + (seconds - i) +" seconds");
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) { }
    }
}
