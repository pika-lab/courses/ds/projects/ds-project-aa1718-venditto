package cli;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "smart-contract", subcommands = {
        PeerSmartContractDeploy.class,
        PeerSmartContractInvoke.class
})
class PeerSmartContractCmd implements Runnable {

    @Option(names = {"--sender", "-s"}, required = true)
    String senderID;

    @Option(names = {"--endorsers", "-e"}, split = ",", required = true)
    String[] endorsers;

    @Option(names = {"--endorsement-policy", "-ep"}, required = true)
    String endorsementPolicy;

    @Option(names = {"--endorser-default-port"}, defaultValue = "55000")
    int defaultEndorserPort;

    @Option(names = {"--committer-host", "-h"}, required = true)
    String committer;

    @Option(names = {"--committer-port", "-p"}, required = true, defaultValue = "26659")
    int committerPort;


    @Override
    public void run() {

    }
}
