package cli;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.protobuf.ByteString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.EndorserClient;
import peer.Peer;
import peer.PeerImpl;
import peer.endorsement.EndorsementPolicy;
import peer.endorsement.EndorsementProposal;
import peer.endorsement.ExpressionEndorsementPolicy;
import peer.endorsement.RequestType;
import peer.proto.InvokePayload;
import peer.proto.Proposal;
import peer.proto.ProposalHeader;
import picocli.CommandLine.*;
import utils.CliUtil;

import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Command(name = "invoke")
public class PeerSmartContractInvoke implements Runnable {

    private final Logger logger = LoggerFactory.getLogger("smart-contract:invoke");

    @ParentCommand
    protected PeerSmartContractCmd peer;

    @Option(names = {"--contract-id", "-i"}, required = true)
    protected String contractID;

    @Option(names = {"--version", "-v"}, required = true)
    protected String version;

    @Option(names={"--args"}, description = "json formatted arguments for invocation")
    private String invokeArgsJSON;

    /**
     *
     * @param e peerid@addr:port
     */
    protected Peer parsePeer(String e) {
        final String[] endorser = e.split("@");
        if (endorser.length < 2) {
            logger.error("invalid endorser provided: " + e + " , aborting deploy.");
            System.exit(-1);
        }
        final String id = endorser[0];
        final String[] addrPort = endorser[1].split(":");
        final String addr = addrPort[0];
        final int port = addrPort.length >= 2 ? Integer.parseInt(addrPort[1]) : peer.defaultEndorserPort;
        return new PeerImpl(id, addr, port);
    }

    protected void submitForEndorsement(Proposal prop) throws IOException  {
        final Set<Peer> endorserPeers = Arrays.stream(peer.endorsers)
                .map(this::parsePeer)
                .collect(toSet());

        final EndorsementPolicy policy = ExpressionEndorsementPolicy.parseFrom(peer.endorsementPolicy);

        try (final EndorserClient client = new EndorserClient(peer.committer, peer.committerPort)) {

            final EndorsementProposal toEndorse = new EndorsementProposal(prop, endorserPeers, policy);

            client.submitForEndorsement(toEndorse).thenRun(() -> logger.info("all endorsement/s received!"));

            Thread.sleep(2500);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        try {
            final JsonObject args = new JsonParser().parse(invokeArgsJSON).getAsJsonObject();

            final ProposalHeader invokeHeader = ProposalHeader.newBuilder()
                    .setInvokerID(ByteString.copyFrom(peer.senderID, "utf-8"))
                    .setTimestamp(Instant.now().toEpochMilli())
                    .setType(RequestType.INVOKE.ordinal())
                    .build();

            final InvokePayload invokePayload = InvokePayload.newBuilder()
                    .setContractID(contractID)
                    .setVersion(version)
                    .setArgs(args.toString()).build();

            final Proposal invokeProposal = Proposal.newBuilder()
                    .setHeader(invokeHeader)
                    .setPayload(invokePayload.toByteString())
                    .build();

            submitForEndorsement(invokeProposal);

        } catch (IOException e) {
            logger.error("error: cause: " + e);
        }
    }
}
