package cli;

import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.PeerClient;
import peer.gossip.Empty;
import peer.gossip.StateEntry;
import peer.gossip.StateUpdate;
import peer.proto.DeployPayload;
import picocli.CommandLine.*;
import utils.ProtoUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Command(name = "debug-deploy", description = "debug command to deploy a smart contract on a peer (no endorsing nor spreading is performed")
public class PeerDeployCmd implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger("peerctl:deploy");

    @Option(names = {"--from", "--sender"}, description = "sender (virtual) peer name", defaultValue = "vPeer")
    private String senderName;

    @ParentCommand
    private PeerCtl peer;

    @Option(names={"--source", "--src", "-s"}, description = "the contract prolog source file")
    private File source;

    @Option(names={"--id", "-i"}, description = "the contract name (ID)")
    private String contractID;

    @Option(names={"--version", "-v"}, description = "the contract version, in the form x.y.z")
    private String version;

    @Option(names = {"--sleep"}, description = "millis to sleep after update sent", defaultValue = "1000")
    private int sleep;

    @Override
    public void run() {
        try {
            final String src = new String(Files.readAllBytes(source.toPath()));

            logger.info("provided source: " + src);

            final ByteString serialized = DeployPayload.newBuilder()
                    .setName(contractID)
                    .setSource(src)
                    .setVersion(version)
                    .build().toByteString();

            final StateEntry se = StateEntry.newBuilder()
                    .setKey("$_" + contractID)
                    .setValue(serialized)
                    .setVersion(0).build();

            final StateUpdate stateUpdate = ProtoUtils.withMetadata(
                    senderName,
                    StateUpdate.newBuilder().addWrite(se)
            ).build();

            final PeerClient client = new PeerClient(peer.host, peer.port);

            logger.info("sending message: " + ProtoUtils.toCompactString(stateUpdate, 2));

            client.getGossipAsyncStub().commitUpdate(stateUpdate, new StreamObserver<Empty>() {
                @Override
                public void onNext(Empty value) { }

                @Override
                public void onError(Throwable t) {
                    logger.error("communication error: cause: " + t.toString());
                }

                @Override
                public void onCompleted() {
                    logger.info("communication completed!");
                }
            });

            Thread.sleep(sleep);

            client.close();

        } catch (IOException e) {
            logger.error("cannot load: " + source);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
