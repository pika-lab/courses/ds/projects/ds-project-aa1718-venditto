package cli;
import peer.endorsement.RequestType;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.PeerClient;
import peer.gossip.StateUpdate;
import peer.proto.*;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParentCommand;
import utils.ProtoUtils;

import java.io.UnsupportedEncodingException;
import java.time.Instant;

@Command(name = "debug-invoke")
public class PeerInvokeCmd implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger("peerctl:invoke");

    @ParentCommand
    private PeerCtl peer;

    @Option(names = {"--from", "--sender"}, description = "sender (virtual) peer name")
    private String senderName;

    @Option(names={"--target", "-t"})
    private String smartContractID;

    @Option(names = {"--version", "-v"}, description = "target smart contract version, if any, in the form x.y.z")
    private String smartContractVersion;

    @Option(names={"--args"}, description = "json formatted arguments for invocation")
    private String invokeArgsJSON;

    @Option(names = {"--sleep"}, description = "millis to sleep after update sent", defaultValue = "1000")
    private int sleep;

    @Override
    public void run() {
        final JsonObject args = new JsonParser().parse(invokeArgsJSON).getAsJsonObject();

        try {
            final ProposalHeader invokeHeader = ProposalHeader.newBuilder()
                    .setInvokerID(ByteString.copyFrom(senderName, "utf-8"))
                    .setTimestamp(Instant.now().toEpochMilli())
                    .setType(RequestType.INVOKE.ordinal())
                    .build();

            final InvokePayload invokePayload = InvokePayload.newBuilder()
                    .setContractID(smartContractID)
                    .setVersion(smartContractVersion)
                    .setArgs(args.toString()).build();

            final Proposal invokeProposal = Proposal.newBuilder()
                    .setHeader(invokeHeader)
                    .setPayload(invokePayload.toByteString())
                    .build();

            final PeerClient client = new PeerClient(peer.host, peer.port);

            client.getEndorserStub().submitForEndorsement(invokeProposal, new StreamObserver<ProposalResponse>() {
                @Override
                public void onNext(ProposalResponse value) {
                    logger.info("\n"+value);
                    final StateUpdate.Builder stateUpdate = StateUpdate.newBuilder();
                    try {
                        final ResponsePayload response = ResponsePayload.parseFrom(value.getPayload());
                        response.getReadList().forEach(stateUpdate::addRead);
                        response.getWriteList().forEach(stateUpdate::addWrite);
                        logger.info("receive r/w set: " + ProtoUtils.toCompactString(stateUpdate.build(), 25));
                    } catch (InvalidProtocolBufferException e) {
                        logger.error("invalid response payload.");
                    }
                }

                @Override
                public void onError(Throwable t) {
                    logger.error("communication error: cause: " + t.toString());
                }

                @Override
                public void onCompleted() {
                    logger.info("communication completed!");
                }
            });

            Thread.sleep(sleep);

            client.close();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
