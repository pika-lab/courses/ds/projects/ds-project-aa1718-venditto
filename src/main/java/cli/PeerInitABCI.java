package cli;

import abci.SmartContractEngineApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.Peer;
import peer.PeerImpl;
import peer.PeerServer;
import picocli.CommandLine.*;

import java.io.IOException;
import java.net.InetAddress;

@Command(name="init-abci", description = "init a peer hosting BC & the core ABCI application.")
public class PeerInitABCI extends PeerInitCmd {

    @ParentCommand
    private PeerCtl peer;

    private final Logger logger = LoggerFactory.getLogger("peerctl:init-abci");

    @Override
    public void run() {

        try {
            final String peerID = name == null ? randName() : name;
            final String localhostName = InetAddress.getLocalHost().getHostAddress();
            final Peer self = new PeerImpl(peerID, localhostName, peer.port);
            loadKnownPeers(self, knownPeers);
            setRoles(self);
            final PeerServer server = new PeerServer(self);
            logger.info("init peer["+ peerID + "] and abci application: " + self);
            server.start();
            final SmartContractEngineApp app = new SmartContractEngineApp(localhostName, peer.port);
            app.listen();
            server.blockUntilShutdown();
        } catch (InterruptedException | IOException e) {
            logger.error("error: cause: " + e);
        }
        logger.info("started abci application..." );
    }
}
