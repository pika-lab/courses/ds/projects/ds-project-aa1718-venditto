package cli;

import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import peer.PeerClient;
import peer.gossip.StateEntry;
import peer.gossip.StateUpdate;
import peer.gossip.UpdateFeedback;
import picocli.CommandLine.*;
import utils.CliUtil;
import utils.ProtoUtils;

import java.io.UnsupportedEncodingException;

@SuppressWarnings("Duplicates")
@Command(name = "commit-update", aliases = {"commit", "cu"})
public class PeerCommitUpdateCmd implements Runnable {

    @ParentCommand
    private PeerCtl peer;

    @Option(names = {"--from", "--sender"}, description = "sender (virtual) peer name", defaultValue = "vPeer")
    private String senderName;

    @Option(names = {"--key", "-K"}, description = "update key")
    private String key;

    @Option(names = {"--value", "-V"}, description = "update value (utf-8 string)")
    private String value;

    @Option(names = {"--version", "-v"}, description = "update version")
    private long version;

    @Option(names = {"--sleep"}, description = "millis to sleep after update sent", defaultValue = "3")
    private int sleep;

    @Override
    public void run() {

        try {
            // build a state asset update
            final StateEntry stateEntry = StateEntry.newBuilder()
                    .setKey(key)
                    .setValue(ByteString.copyFrom(value, "utf-8"))
                    .setVersion(version)
                    .build();

            // add it to a state update message
            final StateUpdate update = ProtoUtils.withMetadata(
                    senderName,
                    StateUpdate.newBuilder().addWrite(stateEntry)
            ).build();

            final PeerClient client = new PeerClient(peer.host, peer.port);

            System.out.println("committing: " + update);

            // connect to peer on specified port and send the update,
            // we don't matter about the response
            StreamObserver<StateUpdate> request = client.getGossipAsyncStub().pushUpdate(new StreamObserver<UpdateFeedback>() {
                @Override
                public void onNext(UpdateFeedback value) {
                    System.out.println("received feedback: " + value);
                }

                @Override
                public void onError(Throwable t) { System.err.println(t); }

                @Override
                public void onCompleted() { System.out.println("completed."); }
            });

            request.onNext(update);
            request.onCompleted();

            CliUtil.startSpinner(sleep, "wait a bit for responses");

            client.close();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
}
