package cli;

import com.google.protobuf.ByteString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.endorsement.RequestType;
import peer.proto.DeployPayload;
import peer.proto.Proposal;
import peer.proto.ProposalHeader;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.File;
import java.nio.file.Files;

@Command(name = "deploy")
public class PeerSmartContractDeploy extends PeerSmartContractInvoke {

    private final Logger logger = LoggerFactory.getLogger("smart-contract:deploy");

    @Option(names = {"--source", "--src", "-s"}, required = true)
    private File sourcePath;

    private void checkSource(String source) {
        if (source.length() <= 0) {
            logger.info("source is empty. aborting deploy;");
            System.exit(-1);
        }
    }

    @Override
    public void run() {
        try {
            final String src = new String(Files.readAllBytes(sourcePath.toPath()));
            checkSource(src);

            final ProposalHeader header = ProposalHeader.newBuilder()
                    .setInvokerID(ByteString.copyFrom(peer.senderID, "utf-8"))
                    .setTimestamp(0)
                    .setType(RequestType.DEPLOY.ordinal())
                    .build();

            final DeployPayload payload = DeployPayload.newBuilder()
                    .setName(contractID)
                    .setSource(src)
                    .setVersion(version)
                    .build();

            final Proposal prop = Proposal.newBuilder()
                    .setHeader(header)
                    .setPayload(payload.toByteString())
                    .build();

            submitForEndorsement(prop);
        } catch (Exception e) {
            logger.error("error: cause: " + e);
        }
    }
}
