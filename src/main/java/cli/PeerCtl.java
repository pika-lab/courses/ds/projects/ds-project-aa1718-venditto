package cli;

import com.google.common.collect.Lists;
import config.GossipConfig;
import config.PeerConfig;
import org.cfg4j.provider.ConfigurationProvider;
import org.cfg4j.provider.ConfigurationProviderBuilder;
import org.cfg4j.source.ConfigurationSource;
import org.cfg4j.source.context.environment.Environment;
import org.cfg4j.source.context.environment.ImmutableEnvironment;
import org.cfg4j.source.context.filesprovider.ConfigFilesProvider;
import org.cfg4j.source.files.FilesConfigurationSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.RunAll;

import java.io.File;
import java.util.Arrays;
import java.util.concurrent.Callable;


@Command(
        name = "peerctl",
        subcommands = {
             PeerInitCmd.class,
             PeerInitABCI.class,
             PeerCommitUpdateCmd.class,
             PeerInvokeCmd.class,
             PeerDeployCmd.class,
             PeerDumpStateCmd.class,
             PeerSmartContractCmd.class
        },
        versionProvider = PeerCtlVersionProvider.class,
        description = "a command line tool to interact with peer instances")
public class PeerCtl implements Callable<Void> {

    private static final String DEFAULT_HOST = "0.0.0.0";
    private static final String DEFAULT_PORT = "55000";
    private static final Logger logger = LoggerFactory.getLogger("peerctl");

    @Option(names = "--version", versionHelp = true, description = "display this help and exit")
    boolean version;

    @Option(names = "--help", usageHelp = true, description = "display this help and exit")
    boolean help;

    @Option(names={"--config", "--cfg", "-c"}, paramLabel = "peerConfigPath", description = "path to peer.yaml")
    private String peerConfig;

    @Option(names={"--host", "-h"}, paramLabel = "hostname", defaultValue = DEFAULT_HOST)
    String host;

    @Option(names={"--port", "-p"}, paramLabel = "port", defaultValue = DEFAULT_PORT)
    int port;

    PeerConfig peerCfg;
    GossipConfig gossipCfg;

    @Override
    public Void call() throws Exception {
        final ConfigFilesProvider provider = () -> Lists.newArrayList(new File("peer.yaml").toPath());

        final ConfigurationSource source = new FilesConfigurationSource(provider);
        final String cfgPath = peerConfig == null ? System.getProperty("user.dir") : peerConfig;
        final Environment environment = new ImmutableEnvironment(cfgPath);

        ConfigurationProvider config = new ConfigurationProviderBuilder()
                .withConfigurationSource(source)
                .withEnvironment(environment)
                .build();

        peerCfg = config.bind("peer", PeerConfig.class);
        gossipCfg = config.bind("peer.gossip", GossipConfig.class);

        logger.info("configuration loaded, ready");

        return null;
    }

    public static void main(String[] args) {

        CommandLine cmd = new CommandLine(new PeerCtl());

        logger.info("*** " + Arrays.toString(cmd.getCommandSpec().version()) + " ***");

        if (cmd.isVersionHelpRequested()) {
            cmd.printVersionHelp(System.out);
            return;
        }

        if(cmd.isUsageHelpRequested()) {
            cmd.usage(System.out);
            return;
        }

        cmd.parseWithHandler(new RunAll(), args);
    }
}
