package cli;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.PeerClient;
import peer.gossip.StateEntry;
import peer.proto.DebugGossipMessage;
import picocli.CommandLine.*;

import javax.xml.bind.DatatypeConverter;

@Command(name = "dump-state", description = "dump state db of a peer")
public class PeerDumpStateCmd implements Runnable {

    @ParentCommand
    private PeerCtl peer;

    @Option(names = {"--target", "-t"}, defaultValue = "world", description = "what state to dump, supported: world, gossip.")
    private String stateTarget;

    private static final Logger logger = LoggerFactory.getLogger("peerctl:dump-state");

    private void dumpWorldState(PeerClient client) {
        final JsonObject state = new JsonObject();
        client.getDebugStub().dumpState(null, new StreamObserver<StateEntry>() {

            private int nEntries = 0;

            @Override
            public void onNext(StateEntry value) {
                nEntries += 1;
                final JsonObject entry = new JsonObject();
                entry.addProperty("value", DatatypeConverter.printHexBinary(value.getValue().toByteArray()));
                entry.addProperty("version", value.getVersion());
                state.add(value.getKey(), entry);
            }

            @Override
            public void onError(Throwable t) {
                logger.error("error: cause: " + t);
            }

            @Override
            public void onCompleted() {
                logger.info("completed. dumped entries: " + nEntries);
                prettyPrintJSON(state);
                System.exit(0);
            }
        });
    }

    private void dumpGossipState(PeerClient client) {
        final JsonObject state = new JsonObject();
        client.getDebugStub().dumpGossipMessageQueue(null, new StreamObserver<DebugGossipMessage>() {
            @Override
            public void onNext(DebugGossipMessage value) {
                final JsonObject gm = new JsonObject();
                gm.addProperty("keepSpreading", value.getSpreading());
                gm.addProperty("spreadTimestamp", value.getMetadata().getTimestamp());
                gm.addProperty("recvTimestamp", value.getRecvTimestamp());
                state.add(value.getMetadata().getId()+"_"+value.getMetadata().getTimestamp(), gm);
            }

            public void onError(Throwable t) {
                logger.error("error: cause: " + t);
            }

            @Override
            public void onCompleted() {
                logger.info("completed.");
                prettyPrintJSON(state);
                System.exit(0);
            }
        });
    }

    private void prettyPrintJSON(JsonObject state) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String prettyJson = gson.toJson(state);
        System.out.println("\r"+prettyJson);
    }

    @Override
    public void run() {
        try( final PeerClient client = new PeerClient(peer.host, peer.port)) {
            if ("gossip".equals(stateTarget)) {
                dumpGossipState(client);
            } else {
                dumpWorldState(client);
            }
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
