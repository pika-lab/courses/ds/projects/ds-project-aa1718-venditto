package utils;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class MapUtils {

    public static <K,V,T,M> Map<T,M> map(
            Map<K,V> aMap,
            Function<? super Map.Entry<K,V>, T> keyMapper,
            Function<? super Map.Entry<K,V>, M> valueMapper) {
        return aMap.entrySet().stream().collect(Collectors.toMap(
            keyMapper,valueMapper));
    }
}
