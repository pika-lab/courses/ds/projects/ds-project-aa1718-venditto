package utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public final class CliUtil {

    private static final int interval = 500;
    private static final String[] frames = new String[]{
            "|»     |",
            "| »    |",
            "|  »   |",
            "|   »  |",
            "|    » |",
            "|     »|",
            "|     «|",
            "|    « |",
            "|   «  |",
            "|  «   |",
            "| «    |",
            "|«     |",
    };

    public static void startSpinner(long timeoutSeconds, String msg) {
        final String[] animation = frames;
        int idx = 0;
        int size = animation.length;
        final long start = System.currentTimeMillis();
        boolean spinning = true;
        while(spinning) {
            try {
                Thread.sleep((long) interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print("\r" + msg + ": " + animation[idx]);
            idx = (idx + 1) % size;
            spinning = !((System.currentTimeMillis() - start) / 1000 >= timeoutSeconds);
        }
        System.out.println("\n");
    }

    public static String ellipsize(String text, int maxVisible, boolean printInfo) {
        if (text == null || text.isEmpty() || text.length() <= maxVisible) return text;
        final String ellipsizedText = text.substring(0, maxVisible) + "...";
        if (printInfo) {
            return ellipsizedText + " (truncated " + (text.length()-maxVisible)+" char.)";
        }
        return ellipsizedText;
    }
}
