package utils;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import peer.Peer;
import peer.PeerImpl;

import java.io.IOException;

public class PeerTypeAdapter extends TypeAdapter<Peer> {
    @Override
    public void write(JsonWriter out, Peer p) throws IOException {
        out.beginObject()
           .value(p.id())
           .value(p.address().getHostName())
           .value(p.address().getPort())
           .endObject();

    }

    @Override
    public Peer read(JsonReader in) throws IOException {
        String id = "unknown";
        String host = "unknown";
        int port = -1;
        in.beginObject();
        while (in.hasNext()) {
            final String name = in.nextName();
            switch (name) {
                case "host":
                    host = in.nextString();
                    break;
                case "port":
                    port = in.nextInt();
                    break;
                case "id":
                    id = in.nextString();
                    break;
                default:
                    in.skipValue();
                    break;
            }
        }
        in.endObject();
        return new PeerImpl(id, host, port);
    }
}
