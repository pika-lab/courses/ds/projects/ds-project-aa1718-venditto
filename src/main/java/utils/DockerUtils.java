package utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class DockerUtils {

    private static final String DOCKER_TASKS_NAMESPACE = "tasks";

    /**
     *   in the context of a docker stack, do a DNS lookup (tasks.<service-name>)
     *   to get the IPs of all the services with a given name.
     *   for more info see: https://docs.docker.com/network/overlay/#container-discovery
     **/
    public static InetAddress[] discoverTasks(String serviceName) {

        try {
            return InetAddress.getAllByName(DOCKER_TASKS_NAMESPACE + "." + serviceName);
        } catch (UnknownHostException e) {
            return new InetAddress[]{};
        }

    }
}
