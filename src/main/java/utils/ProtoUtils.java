package utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import contract.simulation.ReadSetEntry;
import contract.simulation.ReadSetEntryImpl;
import contract.simulation.WriteSetEntry;
import contract.simulation.WriteSetEntryImpl;
import peer.gossip.Metadata;
import peer.gossip.StateEntry;
import peer.gossip.StateUpdate;

import javax.xml.bind.DatatypeConverter;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public final class ProtoUtils {

    public static boolean hasFields(Message m, Set<String> fields) {
        return !Sets.difference(getDefinedFields(m), fields).iterator().hasNext();
    }

    public static Set<String> getDefinedFields(Message m) {
        return m.getAllFields().keySet()
                .stream()
                .filter(m::hasField)
                .map(Descriptors.FieldDescriptor::getName)
                .collect(toSet());
    }

    public static StateUpdate.Builder withMetadata(String id, StateUpdate.Builder b) {
        final long timestamp = Instant.now().toEpochMilli();
        return b.setMetadata(Metadata.newBuilder()
                .setTimestamp(timestamp)
                .setId(id).build());
    }

    public static ReadSetEntry toReadSetEntry(StateEntry entry) {
        return new ReadSetEntryImpl(entry.getKey(), entry.getVersion());
    }

    public static WriteSetEntry<ByteBuffer> toWriteSetEntry(StateEntry entry) {
        return new WriteSetEntryImpl(
                entry.getKey(),
                ByteBuffer.wrap(entry.getValue().toByteArray()));
    }

    public static StateEntry toStateEntry(ReadSetEntry read) {
        return StateEntry.newBuilder()
                .setKey(read.getKey())
                .setVersion(read.getVersion())
                .build();
    }

    public static StateEntry toStateEntry(WriteSetEntry<ByteBuffer> write) {
        final StateEntry.Builder builder = StateEntry.newBuilder();
        builder.setKey(write.getKey());
        if (!write.isMarkedForDelete()) {
            final ByteBuffer val = write.getValue();
            val.rewind();
            builder.setValue(ByteString.copyFrom(val));
        }
        return builder.build();
    }

    public static String toCompactString(Metadata m) {
        return "{ senderID:'" + m.getId() + "', timestamp:" + m.getTimestamp() + " }";
    }

    public static String toCompactString(StateUpdate u, int limitEntries) {
        final StringBuilder sb = new StringBuilder();
        sb.append("{\n\tmeta: ")
                .append(toCompactString(u.getMetadata()));

        final List<StateEntry> updates = Stream.of(u.getReadList(), u.getWriteList())
                .flatMap(Collection::stream)
                .collect(toList());

        final int numEntries = updates.size();
        final int limit = Math.min(limitEntries, numEntries);
        for (int i = 0; i < limit; i++) {
            final StateEntry e = updates.get(i);
            sb.append(",\n\t{ ")
                .append("key:").append(e.getKey()).append(", ")
                .append("value[hex]:").append(DatatypeConverter.printHexBinary(
                    e.getValue().toByteArray())).append(", ")
                .append("version:").append(e.getVersion()).append(" }");
        }
        if (numEntries > limit) {
            sb.append("\n\t...and ").append(numEntries - limit).append(" more entries...");
        }
        sb.append("\n}");

        return sb.toString();
    }
}
