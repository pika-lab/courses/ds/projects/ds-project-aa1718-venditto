package utils;

import java.nio.ByteBuffer;
import java.util.Base64;

public final class ByteUtils {

    public static ByteBuffer bytes(String s) {
        return ByteBuffer.wrap(s.getBytes());
    }

    public static ByteBuffer bytes(int i) { return ByteBuffer.allocate(4).putInt(i); }

    public static ByteBuffer bytes(double d) { return ByteBuffer.allocate(8).putDouble(d); }

    public static ByteBuffer bytes(boolean b) { return ByteBuffer.allocate(1).put(b ? (byte) 0 : (byte) 1); }

    public static int readInteger(ByteBuffer buffer) {
        buffer.rewind();
        return buffer.getInt();
    }

    public static String readString(ByteBuffer buffer) {
        buffer.rewind();
        return new String(readBytes(buffer));
    }

    public static double readDouble(ByteBuffer buffer) {
        buffer.rewind();
        return buffer.getDouble();
    }

    public static boolean readBoolean(ByteBuffer buffer) {
        buffer.rewind();
        return buffer.get() == 0;
    }

    public static String encodeToBase64String(ByteBuffer buffer) {
        return Base64.getEncoder().encodeToString(readBytes(buffer));
    }

    public static ByteBuffer decodeFromBase64String(String base64Encoded) {
        return ByteBuffer.wrap(Base64.getDecoder().decode(base64Encoded));
    }

    public static byte[] readBytes(ByteBuffer buffer) {
        if (buffer.hasArray()) {
            return buffer.array();
        }

        buffer.flip();
        final byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);
        return bytes;
    }

}
