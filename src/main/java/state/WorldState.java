package state;

import com.google.common.base.Equivalence;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import contract.simulation.WriteSetEntry;
import org.h2.mvstore.MVMap;
import org.h2.mvstore.MVStore;
import storage.VersionedKeyValueStorage;
import utils.ByteUtils;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.Map.Entry;
import static java.util.stream.Collectors.toMap;


/**
 * a versioned key value storage that holds the state for the local peer
 */
public class WorldState implements VersionedKeyValueStorage<String, ByteBuffer> {
    
    private MVStore stateDB;
    private MVMap<String, byte[]> data;
    private MVMap<String, Long> version;
    private Equivalence<Long> versionEquiv;

    private final static String DATA_MAP = "data";
    private final static String VERSION_MAP = "version";

    private static final WorldState INSTANCE = new WorldState();

    private WorldState() {
        this.stateDB = MVStore.open(null);
        this.data = stateDB.openMap(DATA_MAP);
        this.version = stateDB.openMap(VERSION_MAP);
        this.versionEquiv = new VersionEquivalence();
    }

    public static WorldState getInstance() { return INSTANCE; }

    @Override
    public void put(String key, ByteBuffer value) {
        this.data.put(key, ByteUtils.readBytes(value));
        this.version.compute(key, (k, v) -> v == null ? 0 : v + 1);
    }

    @Override
    public void putWithVersion(String key, ByteBuffer value, long version) {
        this.data.put(key, ByteUtils.readBytes(value));
        this.version.put(key, version);
    }

    @Override
    public Optional<ByteBuffer> get(String key) {
        final byte[] retrieved = data.get(key);
        return retrieved == null ? Optional.empty() : Optional.of(ByteBuffer.wrap(retrieved));
    }

    @Override
    public long getVersion(String key) {
        return this.version.getOrDefault(key, 0L);
    }

    @Override
    public void del(String key) {
        data.remove(key);
        version.remove(key);
    }

    public ImmutableMap<String, Long> versionSnapshot() {
        return ImmutableMap.copyOf(version);
    }

    /*
    * compute the difference between the versions of THIS state and the OTHER one.
    * the returned map contains:
    *   - entries that have THIS.version > OTHER.version (outdated entries in OTHER state)
    *   - entries only present in THIS state
    * */
    public Map<String, Long> versionDifference(Map<String, Long> otherSnap) {

        final Map<String, Long> localSnap = version;
        final MapDifference<String, Long> diff = Maps.difference(otherSnap, localSnap, versionEquiv);

        final Map<String, Long> mostRecentEntries = diff.entriesDiffering()
                .entrySet().stream()
                .collect(toMap(Entry::getKey, e -> e.getValue().rightValue()));

        return Stream.concat(
                diff.entriesOnlyOnRight().entrySet().stream(),
                mostRecentEntries.entrySet().stream())
                .collect(toMap(Entry::getKey, Entry::getValue));
    }

    public void commit(Set<WriteSetEntry<ByteBuffer>> writes) {
        writes.forEach(w -> {
            if (w.isMarkedForDelete()) {
                del(w.getKey());
            } else {
                put(w.getKey(), w.getValue());
            }
        });
    }

    public Set<Entry<String, byte[]>> dumpRaw() {
        return data.entrySet();
    }

}
