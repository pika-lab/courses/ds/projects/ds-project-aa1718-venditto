package state;

import com.google.common.base.Equivalence;

/*
* equivalence of two key-version entries.
* A (left) differs from B (right) if va < vb
* so keys with a version value less then the other one are considered
* different, otherwise up to date.
* */
public class VersionEquivalence extends Equivalence<Long> {
    @Override
    protected boolean doEquivalent(Long a, Long b) {
        System.out.println("a("+ a +") >= b("+ b +") : " + (a >= b));
        return a >= b;
    }

    @Override
    protected int doHash(Long aLong) {
        return aLong.intValue();
    }
}
