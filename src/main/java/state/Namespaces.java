package state;

public enum Namespaces {

    INTERNAL_SMART_CONTRACT("$");

    private final String namespace;
    private final static String DELIMITER = "_";

    Namespaces(String namespace) {
        this.namespace = namespace;
    }

    public String namespaced(String key) {
        return namespace + DELIMITER + key;
    }

    public static void main(String[] args) {
        System.out.println(Namespaces.INTERNAL_SMART_CONTRACT.namespaced("test"));
    }
}
