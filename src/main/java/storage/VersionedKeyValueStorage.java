package storage;

/**
 * a generic {@link KeyValueStorage} storage that also keeps track of entries version
 */
public interface VersionedKeyValueStorage<K, V> extends KeyValueStorage<K, V> {

    long getVersion(K key);

    void putWithVersion(K key, V value, long version);

}
