package storage;

import java.nio.ByteBuffer;
import java.util.Optional;

/**
 * models a simple key-value storage that supports retrieval of values in binary format
 */
public interface KeyValueStorage<K, V> {

    void put(K key, V value);

    Optional<ByteBuffer> get(K key);

    void del(K key);

}
