package contract;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SmartContractImpl implements SmartContract {

    private String name;
    private String source;
    private String version;


    public SmartContractImpl(String name, String source, String version) {
        this.name = name;
        this.source = source;
        this.version = version;
    }

    @Override
    public String source() {
        return this.source;
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public String version() {
        return version;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result = 31 * result + name.hashCode();
        result = 31 * result + source.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "contract.SmartContract" + "[" + name + "|"+ version + "]";
    }
}
