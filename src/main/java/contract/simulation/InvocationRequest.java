package contract.simulation;

import com.google.common.collect.ImmutableList;

import java.util.*;

/**
 * Encapsulates the request of an invocation against a {@link contract.SmartContract}.
 */
public class InvocationRequest {

    private String receiverId;
    private String op;
    private ImmutableList<Param> params;

    /**
     * @param receiverId the id of the target {@link contract.SmartContract}
     * @param op the name of the operation to invoke
     * @param params the list of the invocation parameters
     */
    private InvocationRequest(String receiverId, String op, ImmutableList<Param> params) {
        this.receiverId = receiverId;
        this.op = op;
        this.params = params;
    }

    public static Builder builder(String id, String op) {
        return new Builder(id, op);
    }

    public String op() { return op; }

    public String contractId() { return receiverId; }

    public ImmutableList<Param> params() { return params; }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("InvocationRequest(")
                .append("op:").append(op)
                .append(",contractID:").append(receiverId);
        params.stream().forEach(p -> sb.append(",").append(p.name).append(":").append(p.value).append("|").append(p.type));
        sb.append(")");
        return sb.toString();
    }

    public static class Builder {

        private String receiverId;
        private String op;
        private List<Param> params;

        Builder(String receiverId, String op) {
            this.receiverId = receiverId;
            this.op = op;
            this.params = new ArrayList<>();
        }

        public Builder paramString(String name, String value) {
            this.params.add(new Param(name, String.class, value));
            return this;
        }

        public Builder paramBoolean(String name, boolean value) {
            this.params.add(new Param(name, boolean.class, value));
            return this;
        }

        public Builder paramNumber(String name, Number number) {
            this.params.add(new Param(name, Number.class, number));
            return this;
        }

        public InvocationRequest build() {
            return new InvocationRequest(receiverId, op, ImmutableList.copyOf(params));
        }
    }

    static public class Param {

        private String name;
        private Class<?> type;
        private Object value;

        Param(String name, Class<?> type, Object value) {
            this.name = name;
            this.type = type;
            this.value = value;
        }

        Object value(){ return value; }
        Class<?> type() {return type; }
        String name() { return name; }

        @Override
        public String toString() {
            return "Param (" + name + " , " + type + " , " + value+")";
        }
    }
}
