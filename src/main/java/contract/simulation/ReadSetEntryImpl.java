package contract.simulation;


public class ReadSetEntryImpl implements ReadSetEntry {

    private String key;
    private long version;

    private static final String NULL_KEY_ERR = "key must be non-null.";
    private static final String NEG_VERSION_ERR = "version must be a positive integer.";

    public ReadSetEntryImpl(String key, long version) {
        if (!checkArgs(key, version)) {
            throw new IllegalArgumentException(NULL_KEY_ERR + " and " + NEG_VERSION_ERR);
        }
        this.key = key;
        this.version = version;
    }

    private boolean checkArgs (String key, long version) {
        return (key != null) && (version >= 0);
    }

    @Override
    public String getKey() {
        return this.key;
    }

    @Override
    public long getVersion() {
        return this.version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReadSetEntryImpl that = (ReadSetEntryImpl) o;
        return key.equals(that.getKey()) &&
                version == that.getVersion();
    }

    @Override
    public int hashCode() {
        int result = 7;
        result = 31 * result + key.hashCode();
        result = 31 * result + ((int)version);
        return result;
    }

    @Override
    public String toString() {
        return "(op=Read, key='" + key + "', version=" + version + ")";
    }
}
