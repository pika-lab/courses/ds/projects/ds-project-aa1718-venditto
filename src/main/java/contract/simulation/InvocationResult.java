package contract.simulation;

import com.google.common.collect.ImmutableSet;

import java.nio.ByteBuffer;
import java.util.Optional;
import java.util.Set;


/**
 * Wraps the result, read-set and write-set generated by the simulation of and {@link InvocationRequest}
 *
 * @param <T> the type of the result, if any
 */
public class InvocationResult<T> {

    private T result;
    private ImmutableSet<ReadSetEntry> readSet;
    private ImmutableSet<WriteSetEntry<ByteBuffer>> writeSet;

    public InvocationResult(
            Set<ReadSetEntry> readSet,
            Set<WriteSetEntry<ByteBuffer>> writeSet) {
        this.readSet = ImmutableSet.copyOf(readSet);
        this.writeSet = ImmutableSet.copyOf(writeSet);
    }

    public void setResult(T result) {
        this.result = result;
    }

    public Optional<T> getResult() {
        return Optional.ofNullable(result);
    }

    public ImmutableSet<ReadSetEntry> getReadSet() {
        return readSet;
    }

    public ImmutableSet<WriteSetEntry<ByteBuffer>> getWriteSet() {
        return writeSet;
    }

    @Override
    public String toString() {
        final StringBuilder b = new StringBuilder();
        b.append("readSet: ");
        b.append(readSet.toString());
        b.append("\n");
        b.append("writeSet: ");
        b.append(writeSet.toString());
        b.append("\n");
        b.append("result: ");
        b.append(result);
        return b.toString();
    }

}
