package contract.simulation;


import alice.tuprolog.*;
import alice.tuprolog.lib.InvalidObjectIdException;
import alice.tuprolog.lib.OOLibrary;
import contract.SmartContract;
import contract.SmartContractRepository;
import contract.state.StateProxy;
import contract.state.StateProxyImpl;
import exceptions.NoSuchSmartContractException;
import exceptions.ParamToTermConversionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import state.WorldState;

import java.lang.Number;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * A basic {@link Invoker} able to simulate an {@link InvocationRequest}
 * against a {@link SmartContract} whose source is a Prolog theory.
 * The simulation happens on a dedicated thread, each time on a new
 * Prolog engine. This invoker is backed by a pool of threads of configurable
 * size.
 */
public class SimplePrologInvoker implements Invoker<String> {

    private ExecutorService executor;

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private static final String PL_OO_LIBRARY = "alice.tuprolog.lib.OOLibrary";
    private static final String PL_RECV_PRED = "receive";
    private static final String PL_INVOKE_TERM = "invoke";
    private static final String PL_RESPONSE_TERM = "Response";
    private static final String PL_STATE_OBJECT = "state";

    /**
     * creates an invoker backed by a single thread.
     */
    public SimplePrologInvoker() {
        this.executor = Executors.newSingleThreadExecutor();
    }

    public SimplePrologInvoker(int threadPoolSize) {
        this.executor = Executors.newFixedThreadPool(threadPoolSize);
    }

    private Term paramToTerm(InvocationRequest.Param p) throws ParamToTermConversionException {
        final Class<?> t = p.type();
        Term tm;
        if (t == java.lang.Number.class) {
            tm = alice.tuprolog.Number.createNumber(p.value().toString());
        } else if (t == String.class) {
            tm = new Struct((String) p.value());
        } else if (t == boolean.class) {
            tm = new Struct((boolean)p.value() ? "true" : "false");
        } else {
            throw new ParamToTermConversionException(
              "cannot convert " + p.toString() + " to a Term, wrong type?"
            );
        }
        return new Struct(p.name(), tm);
    }

    private Term reqToGoal(InvocationRequest i) throws ParamToTermConversionException {

        // build a goal from the submitted request in the form:
        // PL_RECV_PRED([PL_INVOKE_TERM(op), param1(p1), ..., paramN(pN)], PL_RESPONSE_TERM).
        final List<Term> terms = new ArrayList<>();
        terms.add(0, new Struct(PL_INVOKE_TERM, new Struct(i.op())));
        for (InvocationRequest.Param p: i.params()) {
            terms.add(paramToTerm(p));
        }

        return new Struct(PL_RECV_PRED,
                new Struct(terms.toArray(new Term[0])),
                new Var(PL_RESPONSE_TERM));
    }


    @Override
    public CompletableFuture<InvocationResult<String>> execute(InvocationRequest i) throws NoSuchSmartContractException {
        final String id = i.contractId();
        final Optional<SmartContract> maybeSC =
                SmartContractRepository.getInstance().getContract(id);
        final SmartContract sc = maybeSC.orElseThrow(
                () -> new NoSuchSmartContractException(id));
        final CompletableFuture<InvocationResult<String>> futureResult = new CompletableFuture<>();

        executor.submit(() -> {
            final String source = sc.source();
            final Prolog engine = new Prolog();

            try {
                engine.setTheory(new Theory(source));
                logger.info("loading theory: \n" + source + "\n");
                final Library ool = engine.getLibrary(PL_OO_LIBRARY);
                final StateProxy state = new StateProxyImpl(sc.name());

                ((OOLibrary) ool).register(new Struct(PL_STATE_OBJECT), state);
                ((OOLibrary) ool).register(new Struct("stdout"), System.out);

                final Term goal = reqToGoal(i);
                logger.info("trying to solve: " + goal);
                final SolveInfo sol = engine.solve(goal);
                final InvocationResult<String> result =
                        new InvocationResult<>(state.getReadSet(), state.getWriteSet());
                result.setResult(sol.toJSON());
                logger.info(sol.toJSON());
                futureResult.complete(result);
            } catch (InvalidTheoryException | InvalidObjectIdException
                    | ParamToTermConversionException e) {
                System.out.println(e);
                futureResult.completeExceptionally(e);
                logger.error("exception raised during smart contract invocation: ", i, e);
            }
        });

        return futureResult;

    }
}
