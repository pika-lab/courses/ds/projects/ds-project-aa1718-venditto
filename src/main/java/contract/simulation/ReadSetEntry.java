package contract.simulation;

/**
 * Represents a read action requested by a {@link contract.SmartContract}
 * during the simulation of an invocation.
 *
 * @see contract.state.StateProxy
 * @see Invoker
 */
public interface ReadSetEntry {

    String getKey();

    long getVersion();

}
