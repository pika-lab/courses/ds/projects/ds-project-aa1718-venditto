package contract.simulation;

/**
 * Represents a write action performed by a {@link contract.SmartContract}
 * during the simulation of an invocation.
 *
 * @see contract.state.StateProxy
 * @see Invoker
 */
public interface WriteSetEntry<T> {

    String getKey();

    T getValue();

    boolean isMarkedForDelete();

}
