package contract.simulation;

import com.google.gson.annotations.SerializedName;

import java.nio.ByteBuffer;

public class WriteSetEntryImpl implements WriteSetEntry<ByteBuffer> {

    @SerializedName("valueBase64Encoded")
    private ByteBuffer value;

    private String key;
    private boolean delete;

    private static final String NULL_KEY_ERR = "key must be non-null.";

    public WriteSetEntryImpl(String key, ByteBuffer value) {
        if (key == null) {
            throw new IllegalArgumentException(NULL_KEY_ERR);
        }
        this.key = key;
        this.value = value;
        this.delete = (value == null);
    }

    @Override
    public String getKey() {
        return this.key;
    }

    @Override
    public ByteBuffer getValue() {
        return this.value;
    }

    @Override
    public boolean isMarkedForDelete() {
        return this.delete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WriteSetEntryImpl that = (WriteSetEntryImpl) o;
        return key.equals(that.getKey());
    }

    @Override
    public int hashCode() {
        int result = 7;
        result = 31 * result + key.hashCode();
        result = 31 * result + (value == null ? 0: value.hashCode());
        result = 31 * result + (this.delete ? 0 : 1);
        System.out.println("hash<"+key+"-"+value+"-"+delete+">" + " => " + result);
        return result;
    }

    @Override
    public String toString() {
        return "(op=Write, key='" + key + ", value=" + value + ", delete=" + delete + ")";
    }
}
