package contract.simulation;

import exceptions.NoSuchSmartContractException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public interface Invoker<T> {
    /**
     * Asynchronously simulates an {@link InvocationRequest} against the {@link contract.SmartContract}
     * specified in the request itself and wraps the result, read-set and write-set in a {@link InvocationResult}.
     * The 'World State' will NOT be modified by calling this method.
     *
     * @throws NoSuchSmartContractException
     */
    CompletableFuture<InvocationResult<T>> execute(InvocationRequest i) throws NoSuchSmartContractException;

}
