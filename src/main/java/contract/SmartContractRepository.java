package contract;

import com.google.protobuf.InvalidProtocolBufferException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.proto.DeployPayload;
import state.Namespaces;
import state.WorldState;
import utils.ByteUtils;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Optional;

public final class SmartContractRepository {

    private static SmartContractRepository instance = null;

    private Logger logger = LoggerFactory.getLogger("sc-repo");

    public static SmartContractRepository getInstance() {
        if (instance == null) {
            instance = new SmartContractRepository();
        }
        return instance;
    }

    public Optional<SmartContract> getContract(String name) {
        final String namespacedKey = Namespaces.INTERNAL_SMART_CONTRACT.namespaced(name);
        final Optional<ByteBuffer> maybeContract = WorldState.getInstance().get(namespacedKey);
        return maybeContract.map(c -> {
            try {
                final DeployPayload co = DeployPayload.parseFrom(ByteUtils.readBytes(c));
                return new SmartContractImpl(co.getName(), co.getSource(), co.getVersion());
            } catch (InvalidProtocolBufferException e) {
                logger.error("error while retrieving:" + name);
            }
            return null;
        });
    }

    public boolean putContract(SmartContract sc) {
        final String namespacedKey = Namespaces.INTERNAL_SMART_CONTRACT.namespaced(sc.name());
        if (!WorldState.getInstance().get(namespacedKey).isPresent()) {
            final byte[] serialized = DeployPayload.newBuilder()
                    .setName(sc.name())
                    .setSource(sc.source())
                    .setVersion(sc.version())
                    .build().toByteArray();
            WorldState.getInstance().put(namespacedKey, ByteBuffer.wrap(serialized));
            logger.info("deployed smart contract: " + sc.name());
            return true;
        }
        return false;
    }

}
