package contract.state;

import com.google.common.collect.ImmutableSet;
import contract.simulation.ReadSetEntry;
import contract.simulation.ReadSetEntryImpl;
import contract.simulation.WriteSetEntry;
import contract.simulation.WriteSetEntryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import state.WorldState;
import utils.ByteUtils;

import java.nio.ByteBuffer;
import java.util.*;

public class StateProxyImpl implements StateProxy {

    private String namespace;

    private Map<String, ReadSetEntry> readSet;
    private Map<String, WriteSetEntry<ByteBuffer>> writeSet;
    private WorldState worldState;

    private Logger logger;

    public StateProxyImpl(String namespace) {
        this.namespace = namespace;
        this.readSet = new LinkedHashMap<>();
        this.writeSet = new LinkedHashMap<>();
        this.worldState = WorldState.getInstance();
        this.logger = LoggerFactory.getLogger("contract.state.StateProxy["+namespace+"]");
    }

    @Override
    public String namespaced(String key) {
        return this.namespace + key;
    }

    @Override
    public void put(String key, ByteBuffer value) {
        final String namespacedKey = namespaced(key);
        final WriteSetEntry<ByteBuffer> write =
                new WriteSetEntryImpl(namespacedKey, value);
        final WriteSetEntry<ByteBuffer> replaced = this.writeSet.remove(namespacedKey);
        this.writeSet.put(namespacedKey, write);
        this.logger.debug(write.toString() + ", replaced_previous=" + (replaced != null));
    }

    @Override
    public Optional<ByteBuffer> get(String key) {
        final String namespacedKey = namespaced(key);
        final Long currentKeyVersion = this.worldState.getVersion(namespacedKey);
        final ReadSetEntry read =
                new ReadSetEntryImpl(namespacedKey, currentKeyVersion);
        this.readSet.put(namespacedKey, read);
        this.logger.debug(read.toString());
        return this.worldState.get(namespacedKey);
    }

    @Override
    public void del(String key) {
        final String namespacedKey = namespaced(key);
        final WriteSetEntry<ByteBuffer> delete =
                new WriteSetEntryImpl(namespacedKey, null);
        final WriteSetEntry<ByteBuffer> replaced = this.writeSet.remove(namespacedKey);
        this.writeSet.put(namespacedKey, delete);
        this.logger.debug(delete.toString() + ", replaced_previous=" + (replaced != null));
    }

    @Override
    public ImmutableSet<ReadSetEntry> getReadSet() {
        return ImmutableSet.copyOf(this.readSet.values());
    }

    @Override
    public ImmutableSet<WriteSetEntry<ByteBuffer>> getWriteSet() {
        return ImmutableSet.copyOf(this.writeSet.values());
    }

    @Override
    public Optional<Integer> getInt(String key) {
        logger.info("getInt: " + key);
        return get(key).map(ByteUtils::readInteger);
    }

    @Override
    public Optional<Double> getDouble(String key) {
        return get(key).map(ByteUtils::readDouble);
    }

    @Override
    public Optional<String> getString(String key) {
        return get(key).map(ByteUtils::readString);
    }

    @Override
    public Optional<Boolean> getBoolean(String key) {
        return get(key).map(ByteUtils::readBoolean);
    }

    @Override
    public void putInt(String key, int i) {
        put(key, ByteUtils.bytes(i));
    }

    @Override
    public void putDouble(String key, double d) {
        put(key, ByteUtils.bytes(d));
    }

    @Override
    public void putString(String key, String s) {
        put(key, ByteUtils.bytes(s));
    }

    @Override
    public void putBoolean(String key, boolean b) {
        put(key, ByteUtils.bytes(b));
    }

}
