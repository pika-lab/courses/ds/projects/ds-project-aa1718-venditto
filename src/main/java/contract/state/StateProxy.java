package contract.state;

import com.google.common.collect.ImmutableSet;
import contract.simulation.ReadSetEntry;
import contract.simulation.WriteSetEntry;
import storage.KeyValueStorage;

import java.nio.ByteBuffer;
import java.util.Optional;

public interface StateProxy extends KeyValueStorage<String, ByteBuffer> {

    ImmutableSet<ReadSetEntry> getReadSet();

    ImmutableSet<WriteSetEntry<ByteBuffer>> getWriteSet();

    public String namespaced(String key);

    Optional<Integer> getInt(String key);

    Optional<Double> getDouble(String key);

    Optional<String> getString(String key);

    Optional<Boolean> getBoolean(String key);

    void putInt(String key, int i);

    void putDouble(String key, double d);

    void putString(String key, String s);

    void putBoolean(String key, boolean b);

}
