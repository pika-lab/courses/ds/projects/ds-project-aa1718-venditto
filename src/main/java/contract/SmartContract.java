package contract;

public interface SmartContract {

    String source();

    String name();

    String version();
}
