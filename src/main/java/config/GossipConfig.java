package config;

public interface GossipConfig {

    boolean enabled();

    int pushIntervalSeconds();

}
