package config;

import java.util.List;

public interface PeerConfig {

    String name();

    String host();

    int port();

    List<String> roles();

}
