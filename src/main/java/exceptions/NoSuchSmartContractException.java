package exceptions;

public class NoSuchSmartContractException extends Exception {

    private static final String ERR_MSG = "no SmartContract matched id: ";

    public NoSuchSmartContractException(String id) {
        super(ERR_MSG + id);
    }
}
