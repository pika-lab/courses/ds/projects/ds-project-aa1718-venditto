package exceptions;

public class ParamToTermConversionException extends Exception {
    public ParamToTermConversionException(String msg) {
        super(msg);
    }
}
