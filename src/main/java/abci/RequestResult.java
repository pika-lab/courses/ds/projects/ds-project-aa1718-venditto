package abci;

import peer.endorsement.StatusCode;

import java.util.Optional;

public class RequestResult<T> {

    private StatusCode status;
    private String message;
    private T result;

    RequestResult(StatusCode status, String message, T result) {
        this.message = message;
        this.status = status;
        this.result = result;
    }

    public StatusCode status() { return status; }

    public Optional<String> message() { return  Optional.ofNullable(message); }

    public Optional<T> result() { return Optional.ofNullable(result); }

    public boolean isFailure() { return status != StatusCode.OK; }

    public boolean isSuccess() { return !isFailure(); }

}
