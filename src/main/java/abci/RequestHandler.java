package abci;

public interface RequestHandler<T> {

    RequestResult check(T request);

    RequestResult process(T request);

}
