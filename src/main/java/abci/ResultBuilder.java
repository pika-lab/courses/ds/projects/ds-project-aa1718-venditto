package abci;

import peer.endorsement.StatusCode;

public class ResultBuilder<T> {
    private StatusCode status;
    private String message;
    private T result;

    public ResultBuilder(StatusCode status) {
        this.status = status;
    }

    public ResultBuilder setMessage(String message) {
        this.message = message;
        return this;
    }

    public ResultBuilder setResult(T result) {
        this.result = result;
        return this;
    }

    public RequestResult build() {
        return new RequestResult<>(status, message, result);
    }
}
