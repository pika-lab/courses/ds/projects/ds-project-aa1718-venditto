package abci;

import com.github.jtendermint.jabci.api.CodeType;
import com.github.jtendermint.jabci.api.ICheckTx;
import com.github.jtendermint.jabci.api.IDeliverTx;
import com.github.jtendermint.jabci.api.IQuery;
import com.github.jtendermint.jabci.socket.ExceptionListener.Event;
import com.github.jtendermint.jabci.socket.TSocket;
import com.github.jtendermint.jabci.types.*;
import com.google.protobuf.InvalidProtocolBufferException;
import contract.simulation.ReadSetEntry;
import contract.simulation.WriteSetEntry;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import peer.PeerClient;
import peer.gossip.Empty;
import peer.gossip.Metadata;
import peer.gossip.StateUpdate;
import peer.proto.BroadcastProposalTx;
import peer.proto.ProposalResponse;
import peer.proto.ResponsePayload;
import state.WorldState;
import utils.ProtoUtils;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static utils.ProtoUtils.toCompactString;


public class SmartContractEngineApp implements IDeliverTx, ICheckTx, IQuery /*,ICommit*/ {

    private TSocket socket;
    private final WorldState worldState = WorldState.getInstance();
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private PeerClient localPeer;
    private String peerHost;

    private static final String MAIN_THREAD_NAME = "SmartContractEngineThread";
    private static final Long SLEEP = 1000L;

    public SmartContractEngineApp(String peerHost, int peerPort) {
        this.peerHost = peerHost;

        this.socket = new TSocket(
                (socket, event, exception) -> {
                    if (event == Event.SocketHandler_handleRequest) {
                        logger.error(Arrays.toString(exception.getStackTrace()));
                    } else if (event == Event.SocketHandler_readFromStream) {
                        logger.error("error on " + socket.orElse("NONAME") + "-> SocketHandler_readFromStream: " + exception.getMessage());
                    }
                },
                (socketName, count) -> {
                    logger.debug("connect: " + socketName + " , " + count);
                },
                (socketName, count) -> {
                    logger.debug("disconnect: " + socketName + " , " + count);
                }
        );

        this.socket.registerListener(this);

        try {
            localPeer = new PeerClient(peerHost, peerPort);
            logger.info("starting abci server...");
            this.listen();
            localPeer.close();
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public void listen() throws InterruptedException {
        final Thread t = new Thread(
                () -> socket.start(TSocket.DEFAULT_LISTEN_SOCKET_PORT)
        );
        t.setName(MAIN_THREAD_NAME);
        t.start();
        while (true) {
            Thread.sleep(SLEEP);
        }
    }

    /**
     * check if all endorsements present the same payload aka r/w set
     */
    private boolean isProposalTxValid(BroadcastProposalTx tx) {
        if (tx.getEndorsementsList().isEmpty()) {
            return false;
        }

        return tx.getEndorsementsList().stream()
                .map(ProposalResponse::getPayload)
                .distinct()
                .limit(2)
                .count() <= 1;
    }

    private boolean commitStateUpdate(BroadcastProposalTx tx) {

        final StateUpdate.Builder updateBuilder = StateUpdate.newBuilder();

        try {
            ResponsePayload rwSet = ResponsePayload.parseFrom(tx.getEndorsementsList().get(0).getPayload());

            final Set<ReadSetEntry> readSet = rwSet.getReadList().stream()
                    .map(ProtoUtils::toReadSetEntry)
                    .collect(Collectors.toSet());

            final Set<WriteSetEntry<ByteBuffer>> writeSet = rwSet.getWriteList().stream()
                    .map(ProtoUtils::toWriteSetEntry)
                    .collect(Collectors.toSet());

            final boolean isVersionConsistent = readSet.stream().allMatch(
                    r -> r.getVersion() == worldState.getVersion(r.getKey()));

            if (isVersionConsistent) {
                logger.info("read set is validated: commit state update.");
                logger.info("write changes: " + writeSet);
                rwSet.getReadList().forEach(updateBuilder::addRead);
                rwSet.getWriteList().forEach(updateBuilder::addWrite);

                final Metadata meta = Metadata.newBuilder()
                        .setId(tx.getProposal().getHeader().getInvokerID().toStringUtf8())
                        .setTimestamp(tx.getProposal().getHeader().getTimestamp())
                        .build();

                localPeer.getGossipStub().commitUpdate(updateBuilder.setMetadata(meta).build());
                return true;
            }
            logger.warn("update version is not consistent: cannot apply!");

        } catch (InvalidProtocolBufferException e) {
            logger.error("cannot parse r/w set: cause: " + e);
        }
        return false;
    }

    @Override
    public ResponseCheckTx requestCheckTx(RequestCheckTx req) {
        final byte[] txBytes = req.getTx().toByteArray();
        try {
            final BroadcastProposalTx tx = BroadcastProposalTx.parseFrom(txBytes);

            if (isProposalTxValid(tx)) {
                return ResponseCheckTx.newBuilder().setCode(CodeType.OK).build();
            }

        } catch (InvalidProtocolBufferException e) {
            logger.error("invalid transaction: cause: " + e);
        }
        return ResponseCheckTx.newBuilder().setCode(CodeType.BAD).setLog("invalid payload").build();
    }

    @Override
    public ResponseDeliverTx receivedDeliverTx(RequestDeliverTx req) {
        final byte[] txBytes = req.getTx().toByteArray();
        try {
            final BroadcastProposalTx tx = BroadcastProposalTx.parseFrom(txBytes);

            if (isProposalTxValid(tx)) {
                boolean stateCommitOk = commitStateUpdate(tx);
                return ResponseDeliverTx.newBuilder()
                        .setLog("state commit result: " + stateCommitOk)
                        .setCode(stateCommitOk ? CodeType.OK : CodeType.BAD).build();
            }

        } catch (InvalidProtocolBufferException e) {
            logger.error("invalid transaction: cause: " + e);
        }
        return ResponseDeliverTx.newBuilder().setCode(CodeType.BAD).setLog("invalid payload").build();
    }

    @Override
    public ResponseQuery requestQuery(RequestQuery req) {

        return ResponseQuery.newBuilder()
                .setCode(CodeType.BAD)
                .setLog("not implemented.")
                .build();
    }
}