/* example counter sc:

	example:
		receive([invoke('add'), amount(10)], R).

	notes:
		'state' object is registered from within Java.
*/

add(Amount, NewValue) :-
	state <- getInt('counter') returns MaybeValue,
	MaybeValue <- get returns Value,
	NewValue is Value + Amount,
	stdout <- println(NewValue),
	state <- putInt('counter', NewValue).

receive(Message, Response) :-
    Message = [invoke('init')],
    state <- putInt('counter', 0), !
    ;

	Message = [invoke('inc')],
	add(1, _),
	write('inc'), nl, !
	;

	Message = [invoke('add'), amount(Amount)],
	add(Amount, NewValue),
	stdout <- println(NewValue), !
	;

	Message = [invoke('get')],
	state <- getInt('counter') returns Counter,
	Response = ok(Counter).