package state;

import contract.state.StateProxy;
import contract.state.StateProxyImpl;
import org.junit.Test;
import utils.ByteUtils;

import java.nio.ByteBuffer;
import java.util.Optional;

import static org.junit.Assert.*;

public class WorldStateTest {

    private static final String KEY_INT = "keyInt";
    private static final String KEY_DOUBLE = "keyDouble";
    private static final String KEY_BOOLEAN = "keyBoolean";
    private static final String KEY_STRING = "keyString";

    private static final int VALUE_INT = 42;
    private static final double VALUE_DOUBLE = Double.MAX_VALUE;
    private static final boolean VALUE_BOOLEAN = true;
    private static final String VALUE_STRING = "a string!";

    private final static double EPSILON = 0.00001;


    @Test
    public void putNewEntry() {

        WorldState.getInstance().put("newKey", ByteBuffer.allocate(4).putInt(42));
        assertEquals(Optional.of(42), WorldState.getInstance().get("newKey").map(ByteUtils::readInteger));
        assertEquals(0, WorldState.getInstance().getVersion("newKey"));
        WorldState.getInstance().put("newKey", ByteBuffer.allocate(4).putInt(55));
        assertEquals(Optional.of(55), WorldState.getInstance().get("newKey").map(ByteUtils::readInteger));
        assertEquals(1, WorldState.getInstance().getVersion("newKey"));

    }

    @Test
    public void persist() {

        // generate a write-set through a state-proxy
        final StateProxy sp = new StateProxyImpl("test");
        sp.putInt(KEY_INT, VALUE_INT);
        sp.putDouble(KEY_DOUBLE, VALUE_DOUBLE);
        sp.putBoolean(KEY_BOOLEAN, VALUE_BOOLEAN);
        sp.putString(KEY_STRING, VALUE_STRING);

        // assert the world-state is clean
        final WorldState ws = WorldState.getInstance();

        assertEquals(ws.get(KEY_INT), Optional.empty());
        assertEquals(ws.get(KEY_DOUBLE), Optional.empty());
        assertEquals(ws.get(KEY_BOOLEAN), Optional.empty());
        assertEquals(ws.get(KEY_STRING), Optional.empty());

        // commit the changes propose by the write-set
        ws.commit(sp.getWriteSet());

        // verify writes where actually applied
        assertEquals(
                (int) ws.get(sp.namespaced(KEY_INT)).map(ByteUtils::readInteger).get(), VALUE_INT);
        assertEquals(
                ws.get(sp.namespaced(KEY_DOUBLE)).map(ByteUtils::readDouble).get(), VALUE_DOUBLE, EPSILON);
        assertEquals(
                ws.get(sp.namespaced(KEY_BOOLEAN)).map(ByteUtils::readBoolean).get(), VALUE_BOOLEAN);
        assertEquals(
                ws.get(sp.namespaced(KEY_STRING)).map(ByteUtils::readString).get(), VALUE_STRING);

    }
}