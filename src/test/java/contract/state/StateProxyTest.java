package contract.state;

import contract.simulation.ReadSetEntry;
import contract.simulation.ReadSetEntryImpl;
import contract.simulation.WriteSetEntry;
import contract.simulation.WriteSetEntryImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.simple.SimpleLogger;

import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.*;
import static utils.ByteUtils.bytes;

public class StateProxyTest {

    private static final String NS = "state-proxy-test";

    @BeforeClass
    public static void init() {
        System.setProperty(SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "DEBUG");
    }


    @Test(expected = IllegalArgumentException.class)
    public void writeSetEntryNullKey() {
        new WriteSetEntryImpl(null, bytes("value"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void readSetEntryNegVersion() {
        new ReadSetEntryImpl("key", -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void readSetEntryNullKey() {
        new ReadSetEntryImpl(null, 0L);
    }

    @Test
    public void putAndDelete() {
        final StateProxy s = new StateProxyImpl(NS);
        s.put("keyA", bytes("valueA"));
        s.put("keyB", bytes("valueB"));
        s.put("keyA", bytes("latestValueA"));
        s.del("keyB");

        final Set<WriteSetEntry<ByteBuffer>> writeSet = s.getWriteSet();

        assertEquals(2, writeSet.size());
        final Iterator<WriteSetEntry<ByteBuffer>> writes = writeSet.iterator();
        final WriteSetEntry<ByteBuffer> w1 = writes.next();
        assertEquals(NS + "keyA", w1.getKey());
        assertEquals(bytes("latestValueA"), w1.getValue());
        assertFalse(w1.isMarkedForDelete());
        final WriteSetEntry<ByteBuffer> w2 = writes.next();
        assertEquals(NS + "keyB", w2.getKey());
        assertTrue(w2.isMarkedForDelete());
    }

    @Test
    public void get() {
        final StateProxy s = new StateProxyImpl(NS);
        s.get("keyA");
        final Set<ReadSetEntry> readSet = s.getReadSet();
        assertEquals(1, readSet.size());
        final ReadSetEntry r = readSet.iterator().next();
        assertEquals(NS + "keyA", r.getKey());
        assertEquals(0, r.getVersion());
    }
}