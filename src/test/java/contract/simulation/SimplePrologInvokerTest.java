package contract.simulation;

import contract.SmartContract;
import contract.SmartContractImpl;
import contract.SmartContractRepository;
import exceptions.NoSuchSmartContractException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.simple.SimpleLogger;
import utils.ByteUtils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class SimplePrologInvokerTest {

    private static final String SC_NAME = "counter";
    private static final long INVOCATION_TIMEOUT_S = 1;
    private static String theory;

    @BeforeClass
    public static void init() throws IOException {
        final Path source = Paths.get("src","test","resources", "sc_counter.pl");
        theory = new String(Files.readAllBytes(source));
        System.setProperty(SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "DEBUG");
        final SmartContract sc = new SmartContractImpl(SC_NAME, theory, "0.0.0");
        SmartContractRepository.getInstance().putContract(sc);
    }


    @Test
    public void invokeAdd() {
        final Invoker<String> invoker = new SimplePrologInvoker();
        // same as Prolog goal: `receive([invoke('add'), amount(10)], Response).
        // increment the counter (initially of value 0) of 10.
        final int amount = 10;
        final InvocationRequest i = InvocationRequest
                .builder(SC_NAME, "add")
                .paramNumber("amount", amount)
                .build();
        try {
            final InvocationResult<String> res =
                    invoker.execute(i).get(INVOCATION_TIMEOUT_S, TimeUnit.SECONDS);

            System.out.println(res);

            final ReadSetEntry read = res.getReadSet().iterator().next();
            final WriteSetEntry<ByteBuffer> write = res.getWriteSet().iterator().next();

            // read and write set should contain respectively a read and a write to 'counter' key
            assertEquals(SC_NAME+"counter", read.getKey());
            assertEquals(SC_NAME+"counter", write.getKey());
            assertEquals(amount, ByteUtils.readInteger(write.getValue()));
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void invokeGet() {
        final Invoker<String> invoker = new SimplePrologInvoker();

        final InvocationRequest get = InvocationRequest
                .builder(SC_NAME, "get").build();

        try {
            final InvocationResult<String> resGet =
                    invoker.execute(get).get(INVOCATION_TIMEOUT_S, TimeUnit.SECONDS);

            final Optional<String> maybeResultJson = resGet.getResult();

            System.out.println(resGet);

        } catch (TimeoutException | NoSuchSmartContractException | InterruptedException | ExecutionException e) {
            fail(e.getMessage());
        }
    }
}