package peer.endorsement;

import com.google.common.collect.Lists;
import org.junit.Test;
import peer.proto.ProposalResponse;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class ExpressionEndorsementPolicyTest {

    private List<ProposalResponse> mkEndorsements(String... endorsers) {
        return  Lists.newArrayList(endorsers).stream()
                .map(p -> ProposalResponse.newBuilder().setStatus(0).setEndorserID(p).build())
                .collect(Collectors.toList());
    }

    @Test
    public void check() {

        final String expression = "(peer_A && peer_B) || (peer_C && peer_D)";

        final EndorsementPolicy policy = ExpressionEndorsementPolicy.parseFrom(expression);

        assertTrue(policy.check(mkEndorsements("A", "B", "C", "D")));
        assertTrue(policy.check(mkEndorsements("A", "B", "C", "X")));
        assertFalse(policy.check(mkEndorsements("A", "Y", "C", "X")));

    }
}