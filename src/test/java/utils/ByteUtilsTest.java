package utils;

import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.*;

public class ByteUtilsTest {

    private final static double EPSILON = 0.00001;

    @Test
    public void intToFromBytes() {
        final int v = 10;
        final ByteBuffer buff = ByteUtils.bytes(v);
        assertEquals(v, ByteUtils.readInteger(buff));
    }

    @Test
    public void doubleToFromBytes() {
        final double v = Double.MAX_VALUE;
        final ByteBuffer buff = ByteUtils.bytes(v);
        assertEquals(v, ByteUtils.readDouble(buff), EPSILON);
    }

    @Test
    public void booleanToFromBytes() {
        final boolean vt = true;
        final ByteBuffer buffT = ByteUtils.bytes(vt);
        assertTrue(ByteUtils.readBoolean(buffT));

        final boolean vf = false;
        final ByteBuffer buffF = ByteUtils.bytes(vf);
        assertFalse(ByteUtils.readBoolean(buffF));
    }

    @Test
    public void stringToFromBytes() {
        final String s = "StringToBytesAndBack";
        final ByteBuffer buff = ByteUtils.bytes(s);
        assertEquals(s, ByteUtils.readString(buff));
    }

    @Test
    public void bufferEncodeDecodeStringBase64() {
        final String str = "ShouldBeDecoded!";
        final byte[] strBytes = str.getBytes();
        final ByteBuffer buffer = ByteBuffer.wrap(strBytes);
        final String bufferAsBase64 = ByteUtils.encodeToBase64String(buffer);
        final ByteBuffer originalBuffer = ByteUtils.decodeFromBase64String(bufferAsBase64);
        final String shouldEqualOrigString = new String(ByteUtils.readBytes(originalBuffer));
        assertEquals(str, shouldEqualOrigString);
    }
}