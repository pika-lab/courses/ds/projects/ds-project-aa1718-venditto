# Indice
v. 0.2.3
+ 1 [Architettura](#architecture)  
  + 1.1 [Peer](#peer)  
  + 1.2 [Transazioni](#tx)
    + 1.2.1 [Endorsement policy](#endorsement-policy)
  + 1.3 [Stato](#state)
    + 1.3.1 [World State](#world_state)
  + 1.4 [Flusso di transazioni](#tx_flow)
    + 1.4.1 client → proposal → endorsers
    + 1.4.2 endorser → proposal response → client
    + 1.4.3 client → verify proposal response → broadcast tx
+ 2 [Implementazione](#impl)
  + 2.1 [Protocollo di comunicazione](#grpc)
  + 2.2 [Gossip](#gossip)
    + 2.2.1 Gossip-member 
    + 2.2.2 \[TODO\] Consistenza dello stato
  + 2.3 [Smart Contract](#sc)
    + 2.3.1 [API SmartContract](#sc_api)
    + 2.3.2 [API Stato](#sc_state)
    + 2.3.3 [Simulazione](#sc_sim)
+ 3 [Tools & deployment](#tools)
  + 3.1 [Vagrant](#vagrant)
  + 3.2 [Docker](#docker)
    + 3.2.1 [Docker-compose](#docker-compose)
  + 3.3 [peerctl](#peerctl)
+ 4 [Valutazioni e test](#eval)
  + 4.1 [Esempio - testnet](#testnet)
  + 4.2 Test - Gossip: diffusione update 
 
## 1. Architettura del sistema <a name="architecture"></a>

Il sistema è composto da molteplici nodi in comunicazione tra loro, organizzati in due distinti network che svolgono funzioni diverse durante la gestione del flusso di transazioni. La prima rete è composta da nodi che svolgono il ruolo di _endorser_, ovvero hanno la responsabilità di simulare le richieste ricevute da un client e costruire una proposta di transazione contenente eventuali modifiche di stato registrate durante l'esecuzione simulata. La seconda rete è un network p2p Tendermint che mantiene la _blockchain_. I nodi che la compongono hanno il compito di verificare le transazioni e se opportuno modificare lo stato del sistema, propogando poi i cambiamenti ai peer che fanno parte del network di _endorsement_.

<table align="center"><tr><td align="center" width="9999">
<img src="docs/images/arch.png" align="center" width="55%"></td></tr></table>

### 1.1 Peer <a name="peer"></a>
I peer possono avere diversi ruoli, anche sovrapposti, all'interno del sistema:
  - **client**: invia ad uno o più _endorser peer_ una _proposta di transazione_, raccoglie gli _endorsement_ prodotti e se la _policy di endorsement_ è soddisfatta invia _proposal_ e _endorsement_ collezionati al network Tendermint. 
  - **endorser**: alla ricezione di una proposta di transazione da parte di un client, deve simulare l'esecuzione e produrre un elenco di modifiche allo stato in risposta. Mantiene una copia dello stato del sistema. Per mantenere sincronizzato lo stato con quello dei nodi della blockchain, gli *endorser* prendono parte a un protocollo gossip che permette di disseminare gli aggiornamenti di stato.
  - **committer**: sono _validatori_ di un network p2p Tendermint, ogni nodo ospita un applicazione ABCI. Partecipano al consenso, matengono lo stato del sistema, validano le transazioni e modificano lo stato. Quando una modifica viene applicata allo stato propagano l'aggiornamento agli _endorser peer_.
### 1.2 Transazioni <a name="tx"></a>
Il sistema supporta principalmente due tipi di transazioni:
  1. **deploy**: permette di effettuare il deployment di uno smart contract sui peer che partecipano al sistema e abilitare future invocazioni.
  2. **invoke**: permette di invocare un metodo esposto da uno smart contract, precedentemente installato tramite una transazione di _deploy_.
#### 1.2.1 Endorsement policy <a name="endorsement-policy"></a>
Un _endorsement policy_ è un predicato utilizzato per verificare la validità di una proposta di transazione prima di effettuarne il commit. Un client dopo aver ricevuto gli _endorsement_ dai peer ai quali ha sottoposto una richiesta, verifica prima che la policy sia soddisfatta e solo in caso affermativo invia la transazione alla blockchain. Una policy di endorsement può essere specificata come un espressione booleana del tipo `"(peer.A && peer.B) || (peer.A && peer.C)"`, dove ogni *endorser* richiesto viene espresso nella forma `peer.<endorserID>`.  
### 1.3 Stato <a name="state"></a>
Ogni peer che partecipa al sistema mantiene una copia del database dello stato, definito _world state_. I peer che prendono parte al _consenso_ e mantengono la blockchain hanno il compito di rendere effettive eventuali modifiche allo stato, dopo aver validato le transazioni proposte. Per questo motivo dispongono sempre dello stato aggiornato del sistema che devono propagare agli *endorser* peer.
#### 1.3.1 World State <a name="world_state"></a>
Il database dello stato è un _key-value storage_ che mantiene anche un informazione riguardante la versione di ciascuna entry memorizzata, per facilitare verifica e aggiornamento dello stato. Il database dello stato viene utilizzato per memorizzare:
  - gli _SmartContract_
  - lo stato degli _SmartContract_
  - eventuali informazioni di stato del sistema
   
> :memo:
> * Per evitare conflitti tra chiavi, eventuali entry vengono memorizzate in un _namespace_ che coincide con l'identificato dello _SmartContract_ che le gestisce. 
> * Gli _SmartContract_ stessi e altre informazioni di stato afferiscono ad un _namespace_ privato.

## 1.4 Flusso di transazioni <a name="tx_flow"></a>
Di seguito è presentata una descrizione sommaria delle fasi attraversate da una transazione quando fluisce all'interno del sistema.

<table align="center"><tr><td align="center" width="9999">
<img src="docs/images/tx_flow.png" align="center" width="55%"></td></tr></table>

### 1.4.1 client → proposal → endorsers
Un client crea un messaggio di proposta che contiene informazioni sull'originatore della richiesta e tipo di transazione attesa (deploy, invoke) e lo invia ad uno o più _endorser peer_. Il formato del messaggio di _proposal_ è il seguente `Proposal<header, payload>`, dove: 
  - **header**: `Header<type, invokerID, timestamp>` dove `type` indica il tipo tipo di `payload` e `invokerID` è l'identificativo del del client.
  - **payload**: in base al tipo indicato nel campo `type` può essere:
    - `DeployPayload<id, source, version>` dove i campi indicano rispettivamente nome, sorgente e versione dello _SmartContract_ da 'installare' sul sistema.
    - `InvokePayload<id, version, args>` dove `args` indica il metodo da invocare ed i rispettivi parametri di input codificati come stringa in formato JSON.
### 1.4.2 endorser → proposal response → client
Quando un _endorser_ peer riceve un messaggio `Proposal` da parte di un client, deve simularne l'esecuzione e produrre in risposta un messagio `ProposalResponse<endorserID, status, proposalHash, logMsg, endorsements, timestamp>` dove:
  - **endorserID**: contiene il proprio identificativo
  - **status**: un valore intero che indica se l'esecuzione ha avuto esito positivo (`status=0`) o negativo (`status<0`).
  - **proposalHash**: l'hash calcolato su tutto il messaggio `Proposal`, utilizzato dal client per verificare e correlare il response alla proposta.
  - **logMsg** (opzionale): un messaggio di testo per fornire eventuali informazioni riguardo l'esecuzione della proposta
  - **timestamp**: il timestamp registrato al momento della costruzione del messaggio
  - **payload**: `ResponsePayload<writeSet, readSet>`, contiene read e write set produtti dalla simulazione della transazione.
  - **endorsement**: TODO
> :warning: allo stato attuale non è implementato nessun meccanismo crittografico per la garanzia dell'identità degli *endorser*.
 
### 1.4.3 client → verify proposal response → broadcast tx
Il client quando ha collezionato i messaggi `ProposalResponse`
prodotti dai peer _endorser_ ai quali ha inviato la richiesta iniziale, verifica sel l'_endorsement policy_ è soddisfatta. Nel caso in cui lo sia, costruisce un messaggio di `BroadcastProposalTx<Proposal, [ProposalResponse_1, ..., ProposalResponse_n]>` includendo `Proposal` e tutti i `ProposalResponse` ricevuti. Il messaggio prodotto viene poi inviato al network Tendermint per essere validato ed eventualemente modificare lo stato del sistema.
## 2 Implementazione <a name="impl"></a>
### 2.1 Protocollo di comunicazione <a name="grpc"></a>
Le comunicazioni tra nodi avvengono tramite un protocollo rpc, basato sul framework Grpc. In base ai ruoli ricoperti da un peer all'interno del sistema, vengono esposti una serie di servizi:
  - **endorsement service**: Implementato dai peer che possiedo il ruolo di *endorser*, permette di sottomettere proposte di transazione da perte di un client.
  - **gossip service**: Implementato dai nodi che partecipano al protocollo di *data dissemination*, permette ai peer di diffondere gli aggiornamenti di stato.
  - **debug service**: Implementa delle funzionalità che permettono in modalità debug di ispezionare lo stato di un peer.

La codifica dei messaggi è espressa tramite **Protocol Buffers**. L'utilizzo di Grpc per l'implementazione della comunicazione tra peer ha permesso flessibilità e prototipazione veloce. Visto lo stato prototipale del sistema è difficile valutare un eventuale impatto sulle prestazioni dovuto all'overhead che può comportare l'utilizzo del suddetto framework, rispetto all'implementazione di un protocollo di comunicazione _raw_ o ad altre alternative.
### 2.2 Gossip <a name="gossip"></a>
Lo stato del sistema viene modificato solamente nella fase finale del ciclo di vita delle transazioni, ovvero quando vengono verificate e ne viene effettuato il *commit* (TxCommit nell'applicazione ABCI) dai nodi della rete Tendermint. Per questo motivo è stato necessario implementare un meccannismo che permettesse di propagare i cambiamenti di stato anche ai peer *endorser*. Per via della natura distribuita e del tipo di organizzazione p2p dei nodi che compongono il network, l'approccio scelto è stato quello di implementare un protocollo gossip di _data dissemination_. L'algoritmo implementato si basa sul modello SIR, opera in modalità `push` e prevede che la terminazione sia gestità usando una strategia `feedback + random`. Un peer terminerà la diffusione di uno `StateUpdate` con una probailità `1/k` dove `k` rappresenta il numero medio di volte in cui l'update è stato comunicato a un peer che ne era già a conoscenza. Le strategie di `LossOfIterest` e `PeerSelection` possono comunque essere variate. 
#### 2.2.1 Gossip member
Ogni peer che ha un ruolo attivo all'interno della rete di propagazione degli update di stato o `StateUpdate`, esegue un processo che gestisce gli aggiornamenti ricevuti e la loro diffusione ad altri peer. L'esecuzione della diffusione degli aggiornamenti viene effettuatta ad intervalli di tempo configurabili.
#### 2.2.2 [TODO] Consistenza dello stato
L'esecuzione dell'algoritmo descritto sopra non garantisce però la copertura completa di diffusione di un aggiornamento. Per questo motivo si è valutata l'implementazione di un meccaniscanimo _anti-entropy_, da eseguire in modalità `pull/push-pull` ad intervalli di tempo maggiori rispetto alla routine di gossip. Il meccanismo prevederebbe che un peer effettui in maniera attiva una sincronizzazione con un altro peer, aggiornando la sua rapprestazione dello stato con quegli update che non possiede. Visto che scambiare l'intero contenuto del database (supponendo che solo una minima parte non coincida) non sembra essere la soluzione ottimale, sarebbe ragionevole che due peer si scambiassero, in un primo momento, solamente le versioni delle chiavi contenute nei rispettivi database, per poi inviare gli aggiornamenti riguardanti solo gli elementi interessati.
### 2.3 Smart Contract <a name="sc"></a>
Il sistema supporta la realizzazione di SC in linguaggio Prolog. Nello specifico è stata utilizzata la libreria TuProlog e si è sfruttata l'integrazione che abilita con Java.
#### 2.3.1 API SmartContract <a name="sc_api"></a>
L'entry point di uno SC è il predicato `receive(@message, ?Result)`, dove il termine `message` è espresso nel seguente formato: `[invoke(operation_name), param_1_name(value), ..., param_n_name(value)]`.
Di seguito un semplice SC che implementa un contatore:

```prolog
add(Amount, NewValue) :-
    state <- getInt('counter') returns MaybeValue,
    MaybeValue <- get returns Value,
    NewValue is Value + Amount,
    state <- putInt('counter', NewValue).

receive(Message, Response) :-
    Message = [invoke('init')],
    state <- putInt('counter', 0), !
    ;

    Message = [invoke('inc')],
    add(1, _),
    write('inc'), nl, !
    ;

    Message = [invoke('add'), amount(Amount)],
    add(Amount, NewValue), !
    ;

    Message = [invoke('get')],
    state <- getInt('counter') returns Counter,
    Response = ok(Counter).
```
Le possibili invocazioni per ciascun "metodo" dell'esempio sono:
  + **init**: `receive[invoke('init')], _).`
  + **increment**: `receive([invoke('inc')], _).`
  + **add**: `receive([invoke('add'), amount(42)], _).`
  + **get**: `receive([invoke('get')], CounterValue).`

#### 2.3.2 API Stato <a name="sc_state"></a>
Per avere la possibilità di implementare logiche non banali è necessario fornire a uno SC un meccanismo che permetta di interrogare e modificare lo stato del sistema. Va considerato inoltre che le invocazioni di un metodo di uno SC non devono modificare direttamente il *world state*. Per abilitare questo comportamento uno SC ha accesso ad un istanza di uno `StateProxy`, che consente di accedere al db dello stato, intercettanto le operazioni di scrittura ed eventualmente limitando le operazioni di lettura secondo una qualche policy. l'oggetto `StateProxy` espone una serie di metodi`getX/putX`, con X `Int, Double, String e Boolean`.
#### 2.3.3 Simulazione <a name="sc_sim"></a>

<table align="center"><tr><td align="center" width="9999">
<img src="docs/images/simulation.png" align="center" width="55%"></td></tr></table>

L'esecuzione di un invocazione ad uno SC è stata implementata semplicemente come una simulazione, eseguita su un thread dedicato che ospita un engine TuProlog. Le fasi di esecuzione sono:
  1. spawn di un nuovo thread
  2. verifica dell'esistenza dello SC e recupero del relativo codice (teoria Prolog)
  3. istanziazione di un oggetto `StateProxy`, inizializzato con un *namespace* equivalente all'identificativo dello SC
  4. creazione di un engine prolog con la relativa teoria e binding dello `StateProxy` 
  5. conversione dei parametri di input ad un goal prolog
  6. invocazione (risoluzione del goal)
  7. analisi dello `StateProxy` che dopo l'esecuzione contiene `ReadSet`, `WriteSet` ed eventualmente un risultato.
  
Un altra opzione interessante sarebbe quella di appoggiarsi a Docker per l'esecuzione di SC, utilizzando container dedicati. Implementando la comunicazione utilizzando GRPC, sarebbe inoltre possibile sfruttare la codifica dei messaggi già realizzata.

## 3 Tools & deployment <a name="tools"></a>
### 3.1 Vagrant <a name="vagrant"></a>
Per facilitare la build e il testing del sistema è fornito un file di configurazione per una macchina virtuale Vagrant, che automatizza le seguenti operazioni:
  + build distribuzione (*/vagrant/build/peerctl-dist*)
  + build Tendermint
  + build immagine docker sd1718/peer:endorser
  + build immagine docker sd1718/peer:committer
### 3.2 Docker <a name="docker"></a>
Sono state realizzate due distinte immagini docker per i due ruoli principali dei peer nel sistema:
  + **sd1718/peer:endorser**: immagine di un peer con ruolo *endorser* e *gossip_member*.
  + **sd1718/peer:committer**: immagine di un peer che fa parte del network Tendermint. Esegue un nodo Tendermint e un instanza dell'applicazione ABCI.
#### 3.2.1 Docker-compose <a name="docker-compose"></a>
Sono inoltre forniti due file di configurazione per `docker-compose` che permettono di effettuare il deployment di una rete di test. La configurazione prevede:
  + un network di 4 *endorser* in cui ogni peer è a conoscenza di ogni altro peer che partecipa al sistema
  + un network Tendermint composto da 4 validatori / peer

### 3.3 peerctl <a name="peerctl"></a>
Al fine di facilitare l'interazione con il sistema è fornito lo strumento `peerctl`, utilizzabile da linea di comando. Questo tool permette di:
+ inizializzare peer `peerctl init`
+ diffondere update nel sistema per testare il gossip `peerctl commit-update`
+ effettuare deployment di SC `peerctl smart-contract deploy`
+ effettuare invocazioni `peerctl smart-contract invoke`
+ ottenere dump di world-state e gossip-state `peerctl dump-state`

Alcuni esempi di utilizzo:
### Inizializzazione di un peer
inizializza un peer denominato `peerX` con ruolo _endorser_ endorser, in ascolto su tutte le interfacce porta 55000.

  ```shell
  peerctl -h 0.0.0.0 -p 55000 init --name "peerX"
  ```
  
  `config.yaml`
  
  ```yaml
  peer:
    roles:
      - "gossip_member"
      - "endorser"
    gossip:
      enabled: true
      pushIntervalSeconds: 2
  ```
### deployment di uno SmartContract  
```shell
  peerctl smart-contract --sender "clientX" --endorsers "peerA@192.167.11.1","peerB@192.167.11.2" --endorsement-policy "peer.peerA &  
  & peer.peerB" -h 192.167.10.3 deploy -i myContract -v 0.0.1 -s /path/to/my_contract.pl
  ```
  + richiede il **deploy**ment dello smart contract `my_contract.pl` alla versione `0.0.1` con identificativo `myContract`
  + invia la proposta di deployment ai peer `peerA` e `peerB`
  + considera valida la transazione se gli endorsement ricevuti soddisfano la policy `peer.peerA && peer.peerB`
  + nel caso in cui la policy sia rispettata, invia la transazione al peer `192.167.10.3` (membro della rete Tendermint)
  
  
  

## 4 Valutazione e test <a name="eval"></a>
### 4.1 Esempio - testnet <a name="testnet"></a>
init endorsers network

```shell
vagrant up --provision && vagrant ssh
cd /vagrant
docker-compose --file DOCKER/docker-compose-endorsenet.yaml up
...
peer-2    | [main] INFO peerctl - configuration loaded, ready
peer-2    | [main] INFO peerctl:init - init peer: peer_2
peer-2    | [main] INFO peerctl:init - registering role: gossip_member
peer-2    | [main] INFO peerctl:init - registering role: endorser
peer-2    | [main] INFO peerctl:init - local host preferred address is: 192.167.11.2
peer-2    | [main] INFO peer-manager - registered: peer_1@/192.167.11.1:55000
peer-2    | [main] INFO peer-manager - registered: peer_3@/192.167.11.3:55000
peer-2    | [main] INFO peer-manager - registered: peer_4@/192.167.11.4:55000
peer-2    | [main] INFO server[peer_2] - ready, listening on port: 55000
...
peer-1    | [main] INFO peerctl:init - init peer: peer_1
peer-1    | [main] INFO peerctl:init - local host preferred address is: 192.167.11.1
...
peer-3    | [main] INFO peerctl:init - init peer: peer_3 
peer-3    | [main] INFO peerctl:init - local host preferred address is: 192.167.11.3
...
peer-4    | [main] INFO peerctl:init - init peer: peer_4  
peer-4    | [main] INFO peerctl:init - local host preferred address is: 192.167.11.4
```
inizializza il network Tendermint

```shell
cd /home/vagrant/go/src/github.com/tendermint/tendermint/
docker-compose --file docker-compose-commitnet.yaml up
```

effettua il deploy dello SmartContract *counter*

```shell
cd /vagrant/build/peerctl-dist/
./bin/peerctl smart-contract --sender "client" --endorsers "peer_1@192.167.11.1","peer_2@192.167.11.2" --endorsement-policy "peer.peer_1 && peer.peer_2" -h 192.167.10.3 deploy -i counter -v 0.0.1 -s /vagrant/src/test/resources/sc_counter.pl
[ForkJoinPool.commonPool-worker-1] INFO peer.endorsement.ExpressionEndorsementPolicy - checking: [
  {
    endorserID: "peer_1"  
    proposal_hash: ...
    payload: ...  
    timestamp: 1546003507017
  },
  {
    endorserID: "peer_2"  
    proposal_hash: ...  
    payload: ...  
    timestamp: 1546003507058  
  }
]
[ForkJoinPool.commonPool-worker-1] INFO peer.EndorserClient - endorsement policy satisfied, broadcasting tx.  
[grpc-default-executor-0] INFO peer.BroadcastTxClient - broadcastTx response: check_tx { }  deliver_tx { }
completed broadcastTx!
```

verifica che effettivamente lo SC *counter* sia stato "installato" sul sistema

```shell
./bin/peerctl -h 192.167.11.1 dump-state -t world
[grpc-default-executor-0] INFO peerctl:dump-state - completed. dumped entries: 1  
{  
 "$_counter": {  
    "value": ...,  
    "version": 0  
  }  
}
```

inizializzazione dello SC *counter*

```shell
./bin/peerctl smart-contract --sender "client" --endorsers "peer_1@192.167.11.1","peer_2@192.167.11.2" --endorsement-policy "peer.peer_1 && peer.peer_2" -h 192.167.10.3 invoke -i counter --args '{ "op":"init" }' -v 0.0.1
```

invocazione del metodo 'inc' dello SC

```shell
./bin/peerctl smart-contract --sender "client" --endorsers "peer_1@192.167.11.1","peer_2@192.167.11.2" --endorsement-policy "peer.peer_1 && peer.peer_2" -h 192.167.10.3 invoke -i counter --args '{ "op":"add","amount": 42 }' -v 0.0.1
```

> lato endorser peer_1:
>  
> ```shell
> ...
> peer-1    | [pool-3-thread-1] INFO contract.simulation.SimplePrologInvoker - trying to solve: receive([invoke(add),amount(42)],Response)  
> peer-1    | [pool-3-thread-1] INFO contract.state.StateProxy[counter] - getInt: counter
> peer-1    | [pool-3-thread-1] INFO contract.state.StateProxy[counter] - putInt: counter <- 42    
> peer-1    | [grpc-default-executor-1] INFO endorse-handler -> generated r/w-set:  
> peer-1    | read {  
> peer-1    |     key: "countercounter"  
> peer-1    | }  
> peer-1    | write {  
> peer-1    |     key: "countercounter"  
> peer-1    |     value: "\\000\\000\\000*"  
> peer-1    | }  
> peer-1    |
> ...
> ```

verifica dell'effettivo esito dell'operazione

```shell
./bin/peerctl -h 192.167.11.1 dump-state -t world
[grpc-default-executor-0] INFO peerctl:dump-state - completed. dumped entries: 2 
{  
 "$_counter": { # private namespace $_
    "value": ...,  
    "version": 0  
  },
  "countercounter": { # namespaced key
    "value": "0000002A", # 42 
    "version": 1
  }
}
```

### 4.2 Test - Gossip: diffusione update

+ test effettuato su una rete `docker-compose`

| peer          | received (s)  | first receiver  |
| :------------ |-------------:| :--------------:|
|10.0.0.2|11.040| |
|10.0.0.3|23.759| |
|10.0.0.4|6.727| |
|10.0.0.5|8.446| |
|10.0.0.6|4.414| |
|10.0.0.7|8.608| |
|10.0.0.8|7.770| |
|10.0.0.9|6.979| |
|10.0.0.10|1.695| |
|10.0.0.11|11.381| |
|10.0.0.12|12.980| |
|10.0.0.13|9.336| |
|10.0.0.14|13.702| |
|10.0.0.15|11.040| |
|10.0.0.16|9.044| |
|10.0.0.17|16.949| |
|10.0.0.18|3.644| |
|10.0.0.19|9.292| |
|10.0.0.20|3.287| |
|10.0.0.21|13.987| |
|10.0.0.22|9.768| |
|10.0.0.23|5.112| |
|10.0.0.24|12.479| |
|10.0.0.25|0.469| X |
|10.0.0.26|5.204| |
|10.0.0.27|12.681| |


| parameters and results          |   |
| :------------ |-------------:|
| # peers | 25 |
| k      | 5  |
|gossip-timeout (s)| 5 |
|avg recv  time (s)|  9.22   |
|max recv  time (s)| 23.76  |

## Riferimenti
- [DI MARZO SERUGENDO, Giovanna, GLEIZES, Marie-Pierre, KARAGEORGOS, Anthony, (eds.). Self-Organizing Software: From Natural to Artificial Adaptation. Berlin : Springer, 2011.](https://www.springer.com/us/book/9783642173479)
- [Alberto Montresor, Gossip and Epidemic Protocols](http://disi.unitn.it/~montreso/ds/papers/montresor17.pdf)
- [Blockchain & Smart Contracts Basics and Perspectives for MAS](https://www.slideshare.net/gciatto/blockchain-smart-contracts-basics-and-perspectives-for-mas)
- [Hyperledger Fabric: A Distributed Operating System for Permissioned Blockchains](https://arxiv.org/abs/1801.10228)
- [Architecture Explained — hyperledger-fabricdocs master documentation](https://hyperledger-fabric.readthedocs.io/en/release-1.3/arch-deep-dive.html?)
- [Blockchain explored](https://www.slideshare.net/ibmsverige/blockchain-explored)
