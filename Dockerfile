FROM anapsix/alpine-java
WORKDIR /peer
ADD build/peerctl-dist/ /peer
ADD DOCKER/endorser_peer/* /peer/
EXPOSE 55000
EXPOSE 26656
EXPOSE 26658
EXPOSE 26659
ENTRYPOINT ["bin/peerctl"]
