#!/bin/bash

REPO=github.com/tendermint/tendermint

sh /vagrant/gradlew -p /vagrant installShadowDist

cd "$GOPATH/src/$REPO"
cp /vagrant/DOCKER/commitnet/Dockerfile.committer DOCKER/
cp /vagrant/DOCKER/commitnet/docker-compose-commitnet.yaml .
cp -r /vagrant/DOCKER/commitnet .
# build a custom tendermint image embedding tendermint + peer:committer
sudo docker build -t sd1718/peer:committer -f DOCKER/Dockerfile.committer DOCKER/
cd "$GOPATH/src/$REPO/commitnet"
bash localnet.sh

# build peer:endorser image
cd "/vagrant"
sudo docker build -t sd1718/peer:endorser .
