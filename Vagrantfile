# -*- mode: ruby -*-
# vi: set ft=ruby :

$install_docker_and_java = <<-SHELL

    sudo apt-get update
    sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common

    # get docker
    if [ ! -f /vagrant/docker.lock ]; then
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        sudo add-apt-repository \
            "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
            $(lsb_release -cs) \
            stable"
        sudo apt-get -y update
        sudo apt-get install -y docker-ce
        sudo usermod -aG docker $USER

        # get docker-compose
        sudo curl -L https://github.com/docker/compose/releases/download/1.23.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
        sudo chmod +x /usr/local/bin/docker-compose
        touch "/vagrant/docker.lock"
    else
        echo "skipping Docker install: delete docker.lock to reinstall."
    fi

    if [ ! -f /vagrant/java.lock ]; then
        #install java 8 JRE
        sudo apt install -y openjdk-8-jdk
        touch "/vagrant/java.lock"
    else
        echo "skipping Java8 jdk install. delete java.lock to reinstall."
    fi
  SHELL
  
$install_tendermint =  <<-SHELL
    sudo service docker restart
    if [ ! -f /vagrant/tendermint.lock ]; then
        #install tendermint and build docker images
        bash /vagrant/install_tendermint.sh
        touch "/vagrant/tendermint.lock"
    else
        echo "skipping Tendermint install. delete tendermint.lock to reinstall."
    fi

SHELL
  


Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial64"

  config.vm.provider "virtualbox" do |v|
    v.memory = 4096
    v.cpus = 4
  end
    
  config.vm.network "private_network", ip: "192.168.50.2"
  config.vm.provision "shell", inline: $install_docker_and_java, privileged: false
  config.vm.provision "shell", inline: $install_tendermint, privileged: false
  config.vm.provision "shell", path: "build_docker_imgs.sh", privileged: false
  
end 
