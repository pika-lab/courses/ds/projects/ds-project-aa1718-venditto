#!/bin/bash

# clean up node homes and workidir
sudo rm -rf node0 node1 node2 node3 peer
# populate 4 nodes
tendermint testnet --v 4 --o . --populate-persistent-peers --starting-ip-address 192.167.10.2
mkdir peer
# copy scripts to workdir
cp {peer.yaml,run.sh,peers.json} peer/
# copy tendermint binary to workidr
cp ../build/tendermint peer/
# copy peerctl.jar 
cp -r /vagrant/build/peerctl-dist/* peer/
# set permissions
sudo chmod -R 777 node0 node1 node2 node3
