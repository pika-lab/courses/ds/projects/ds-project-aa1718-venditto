#!/bin/bash
echo $TMHOME
echo $ID
ID=${ID:-0}
export TMHOME="/tendermint/node${ID}"
echo $TMHOME

./bin/peerctl init-abci --name "node_${ID}" --known-peers peers.json &
sleep 5
/tendermint/peer/tendermint node --rpc.grpc_laddr="tcp://0.0.0.0:26659"
